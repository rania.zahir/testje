﻿using IP.BL.Domain.Flows.FlowComponents.Questions.API;
using Microsoft.Extensions.Options;

namespace IP.BL.Domain.Flows.FlowComponents.Questions;

public class Question : FlowItem
{
    public QuestionType Type { get; set; }
    public string Prompt { get; set; }
    public List<Option> Options { get; set; }
    public bool IsOpen { get; set; } 
    
    
}