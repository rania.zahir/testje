﻿namespace IP.BL.Domain.Flows.FlowComponents.Questions;

public enum QuestionType
{
    Multiple, Range, Open, Single
}