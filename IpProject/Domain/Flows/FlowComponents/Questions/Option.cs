namespace IP.BL.Domain.Flows.FlowComponents.Questions;

public class Option
{
    public Guid Id { get; set; }
    public string Label { get; set; }
    public int Value { get; set; }
    public Guid QuestionId { get; set; }
    public Question Question { get; set; }
}