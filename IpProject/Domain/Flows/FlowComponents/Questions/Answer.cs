﻿using IP.BL.Domain.Flows.FlowComponents.Questions.API;

namespace IP.BL.Domain.Flows.FlowComponents.Questions;

public class Answer : IAnswer
{
    public Guid Id { get; set; }
    public Question Question { get; set; }
    public ICollection<Option> Options { get; set; }
    public string OpenText { get; set; }
}