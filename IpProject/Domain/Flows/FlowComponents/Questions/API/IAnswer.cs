﻿namespace IP.BL.Domain.Flows.FlowComponents.Questions.API;

public interface IAnswer
{
    public Guid Id { get; set; }
    public Question Question { get; set; }

}