﻿using System.Collections.ObjectModel;
using IP.BL.Domain.Flows.FlowComponents.Questions;

namespace IP.BL.Domain.Flows.FlowComponents;

public class Subtheme
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public Guid FlowId { get; set; }
    public Flow Flow { get; set; }
    public ICollection<FlowItem> FlowItems { get; set; }
    public InfoComponent Info { get; set; }
}