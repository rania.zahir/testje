﻿namespace IP.BL.Domain.Flows.FlowComponents;

public class InfoComponent
{
    public Guid Id { get; set; }
    public string Text { get; set; }
}