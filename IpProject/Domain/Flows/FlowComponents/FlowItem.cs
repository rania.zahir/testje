﻿namespace IP.BL.Domain.Flows.FlowComponents;

public abstract class FlowItem
{
    public Guid Id { get; set; }
    public Guid SubthemeId { get; set; }
    public Subtheme Subtheme { get; set; }
}