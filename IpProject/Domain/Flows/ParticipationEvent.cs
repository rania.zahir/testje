﻿using System.ComponentModel.DataAnnotations;

namespace IP.BL.Domain.Flows;

public class ParticipationEvent
{
    [Key]
    public Guid Id { get; set; }
    public DateTime BeginTime { get; set; }
    public DateTime EndTime { get; set; }

    public ParticipationEvent()
    {
        BeginTime = DateTime.Now;
    }
}