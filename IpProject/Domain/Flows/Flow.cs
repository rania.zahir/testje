﻿using IP.BL.Domain.Flows.FlowComponents;
using IP.BL.Domain.User;

namespace IP.BL.Domain.Flows;

public class Flow
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string Type { get; set; }
    public string Description { get; set; }
    public Guid ProjectId { get; set; }
    public Project Project { get; set; }
    public ICollection<Subtheme> Subthemes { get; set; }
}