using IP.BL.Domain.Flows;
using IP.BL.Domain.Flows.FlowComponents;
using IP.BL.Domain.User;

namespace IP.BL.Domain;

public class Project
{
    public Guid Id { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public Guid PlatformId { get; set; }
    public Platform Platform { get; set; }
    public ICollection<Flow> Flows { get; set; }
}