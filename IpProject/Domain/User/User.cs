﻿using System.ComponentModel.DataAnnotations;

namespace IP.BL.Domain.User;

public class User
{
    public Guid Id { get; set; }
    public string Email { get; set; }

    public User()
    {
    }
    
    public User(string email)
    {
        this.Email = email;
    }   
}
