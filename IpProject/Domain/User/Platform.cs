using IP.BL.Domain.Flows;
using Microsoft.AspNetCore.Identity;

namespace IP.BL.Domain.User;

public class Platform
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public ICollection<Project> Projects { get; set; }
}
