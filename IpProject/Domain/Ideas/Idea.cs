﻿namespace IP.BL.Domain.Ideas;

public class Idea
{
    public Guid Id { get; set; }
    public string Title {get; set; }
    public string Text {get; set; }
}