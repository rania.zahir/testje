﻿namespace UI_MVC.Models;

public class OptionViewModel
{
    public Guid Id { get; set; }
    public string Label { get; set; }
    public int Value { get; set; }

}