using IP.BL.Domain;
using IP.BL.Domain.User;

namespace UI_MVC.Models;

public class DashboardViewModel
{
    public IEnumerable<User> Users { get; set; }
    public IEnumerable<Platform> Platforms { get; set; }
    public IEnumerable<Project> Projects { get; set; }
}