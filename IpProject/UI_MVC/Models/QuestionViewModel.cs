﻿using IP.BL.Domain.Flows.FlowComponents.Questions;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace UI_MVC.Models;

public class QuestionViewModel
{
    public Guid Id { get; set; } // Voeg Id toe
    public QuestionType Type { get; set; }
    public string Prompt { get; set; }
    public List<OptionViewModel> Option { get; set; }
    public bool IsOpen { get; set; } 
    public Guid SubthemaId { get; set; }
    public List<SelectListItem> Subthema { get; set; }
    
}