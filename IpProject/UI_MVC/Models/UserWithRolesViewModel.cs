using Google.Api.Gax;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace UI_MVC.Models;

public class UserWithRolesViewModel
{
    public IdentityUser User { get; set; }
    public IList<string> Roles { get; set; }
}