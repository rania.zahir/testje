﻿namespace IpProject.UI.MVC.Models.User;

public class UserModel
{
    public Guid Id { get; set; }
    public string Email { get; set; }
}