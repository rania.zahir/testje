using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace UI_MVC.Models;

public class NewProjectViewModel
{
    [Required]
    public string Title { get; set; }

    [Required]
    public string Description { get; set; }
    public Guid PlatformId { get; set; }
    public List<SelectListItem> Platforms { get; set; }
}