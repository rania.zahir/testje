using IP.BL.Domain.Flows.FlowComponents;

namespace UI_MVC.Models;

public class SubthemeFlowViewModel
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public ICollection<FlowItem> FlowItems { get; set; }
    public string ModeString { get; set; }
}