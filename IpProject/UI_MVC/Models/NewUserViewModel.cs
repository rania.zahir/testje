using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace UI_MVC.Models;

public class NewUserViewModel
{
    [Required]
    public string UserName { get; set; }

    [Required]
    [EmailAddress]
    public string Email { get; set; }

    [Required]
    [DataType(DataType.Password)]
    public string Password { get; set; }
    public string SelectedRole { get; set; }
    public List<string> AvailableRoles { get; set; }
    
    
    public Guid PlatformId { get; set; }
    public List<SelectListItem> Platforms { get; set; }
}