using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace UI_MVC.Models;

public class NewSubthemeViewModel
{
    [Required]
    public string Name { get; set; }
    [Required]
    public string Description { get; set; }
    public Guid FlowId { get; set; }
    public List<SelectListItem> Flows { get; set; }
    
    
}