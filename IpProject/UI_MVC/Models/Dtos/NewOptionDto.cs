﻿using IP.BL.Domain.Flows.FlowComponents.Questions;

namespace IpProject.UI.MVC.Models.Dtos;

public class NewOptionDto
{
    public string Label { get; set; }
    public int Value { get; set; }
    public Guid QuestionId { get; set; }
    public Question Question { get; set; }
}