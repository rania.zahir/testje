namespace IpProject.UI.MVC.Models.Dtos;

public class NewSubthemeDto
{
    public string Title { get; set; }
    public string Description { get; set; }
}