﻿using IP.BL.Domain.Flows.FlowComponents.Questions;

namespace IpProject.UI.MVC.Models.Dtos;

public class AnswerDto
{
    public string QuestionId { get; set; }
    public List<string> Options { get; set; }
    public string OpenText { get; set; }
}