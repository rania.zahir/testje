﻿namespace IpProject.UI.MVC.Models.Dtos;

public class OptionDto
{
        public Guid Id { get; set; }
        public string Label { get; set; }
        public int Value { get; set; }
}