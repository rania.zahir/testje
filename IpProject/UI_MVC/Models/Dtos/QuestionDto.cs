﻿using IP.BL.Domain.Flows.FlowComponents.Questions;

namespace IpProject.UI.MVC.Models.Dtos;

public class QuestionDto
{
    public Guid Id { get; set; }
    public QuestionType Type { get; set; }
    public string Prompt { get; set; }
    public List<OptionDto> Options { get; set; }
}