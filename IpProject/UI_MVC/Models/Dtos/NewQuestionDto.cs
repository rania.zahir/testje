﻿using System.Runtime.CompilerServices;
using IP.BL.Domain.Flows.FlowComponents;
using IP.BL.Domain.Flows.FlowComponents.Questions;

namespace IpProject.UI.MVC.Models.Dtos;

public class NewQuestionDto
{
    public Guid Id { get; set; }
    public QuestionType Type { get; set; }
    public string Prompt { get; set; }
    public IEnumerable<NewOptionDto> Options { get; set; }
    public bool IsOpen { get; set; }
    public Guid SubthemeId { get; set; }
    
    
}