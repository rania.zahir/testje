﻿using IP.BL.Domain.Ideas;

namespace IpProject.UI.MVC.Models.Dtos;

public class IdeaDto
{
    public IEnumerable<Idea> Ideas { get; set; } = new List<Idea>();
}