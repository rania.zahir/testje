using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace UI_MVC.Models;

public class NewPlatformViewModel
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    
}