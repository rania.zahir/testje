﻿using IP.BL.API;
using IP.BL.Managers;
using IP.DAL.API;
using IP.DAL.EF;
using IP.DAL.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace IpProject.UI.MVC;

public class Startup
{
    
    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }
    private IConfiguration Configuration { get; }
    
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddDbContext<AppDbContext>(optionsBuilder =>
            optionsBuilder.UseNpgsql(Configuration.GetConnectionString("DefaultConnection")));
        services.AddDefaultIdentity<IdentityUser>(options => options.SignIn.RequireConfirmedAccount = false)
            .AddRoles<IdentityRole>().AddEntityFrameworkStores<AppDbContext>();
        
        services.AddControllersWithViews();
        services.AddScoped<DbInit>();
        services.AddScoped<UnitOfWork>();
        services.AddScoped<IAnswerManager, AnswerManager>();
        services.AddScoped<ISubthemeRepository, SubthemeRepository>();
        services.AddScoped<ISubthemeManager, SubthemeManager>();
        services.AddScoped<IAnswerRepository, AnswerRepository>();
        services.AddScoped<IFlowManager, FlowManager>();
        services.AddScoped<IFlowRepository, FlowRepository>();
        services.AddScoped<IUserManager, UserManager>();
        services.AddScoped<IUserRepository, UserRepository>();
        services.AddScoped<IQuestionRepository, QuestionRepository>();
        services.AddScoped<IIdeasRepository, IdeasRepository>();
        services.AddScoped<IIdeasManager, IdeasManager>();

        services.AddTransient<DbInit>();
        services.AddControllersWithViews();
        services.AddControllers().AddXmlSerializerFormatters();

        // Add Razor Pages
        services.AddRazorPages();
        
        // Add CloudStorageService
        //services.AddSingleton<CloudStorageService>();
    }

    //test
     public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        // Configure the HTTP request pipeline.
        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
        }
        else
        {
            app.UseExceptionHandler("/Home/Error");
            // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            app.UseHsts();
        }
        
        app.UseHttpsRedirection();
        app.UseStaticFiles();

        app.UseRouting();
        app.UseAuthentication();
        app.UseAuthorization();
            
        using var scope = app.ApplicationServices.CreateScope();
        var services = scope.ServiceProvider;
        
        var context = services.GetRequiredService<AppDbContext>();
        if (context.CreateDatabase(dropDatabase: true))
        {
            var httpContextAccessor = services.GetRequiredService<IHttpContextAccessor>();
            //httpContextAccessor.HttpContext.Session.Clear();
            
            var init = services.GetRequiredService<DbInit>();
            init.Run();
            //add users&roles
            var userManager = services.GetRequiredService<UserManager<IdentityUser>>();
            //SeedUsers(userManager);
            var roleManager = services.GetRequiredService<RoleManager<IdentityRole>>();
            SeedUsers(userManager, roleManager);
            
        }
        
        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllerRoute(
                name: "default",
                pattern: "{controller=Home}/{action=Index}/{id?}");
            
            endpoints.MapRazorPages();
        });
        
        
    }
    
    void SeedUsers(UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager)
    {
        var userRole = new IdentityRole(CustomIdentityConstants.UserRole);
        roleManager.CreateAsync(userRole).Wait();
        var managerRole = new IdentityRole(CustomIdentityConstants.ManagerRole);
        roleManager.CreateAsync(managerRole).Wait();
        var adminRole = new IdentityRole(CustomIdentityConstants.AdminRole);
        roleManager.CreateAsync(adminRole).Wait();
        
        var admin1 = new IdentityUser
        {
            UserName = "admin1@kdg.be",
            Email = "admin1@kdg.be"
        };
        userManager.CreateAsync(admin1, "Password1!").Wait();
        var admin2 = new IdentityUser
        {
            UserName = "admin2@kdg.be",
            Email = "admin2@kdg.be"
        };
        userManager.CreateAsync(admin2, "Password1!").Wait();
        var admin3 = new IdentityUser
        {
            UserName = "admin3@kdg.be",
            Email = "admin3@kdg.be"
        };
        userManager.CreateAsync(admin3, "Password1!").Wait();
        var manager1 = new IdentityUser {
            UserName = "manager1@kdg.be",
            Email = "manager1@kdg.be"
        };
        userManager.CreateAsync(manager1, "Password1!").Wait();
        
        var manager2 = new IdentityUser{
            UserName = "manager2@kdg.be",
            Email = "manager2@kdg.be"
        };
        userManager.CreateAsync(manager2, "Password1!").Wait();
        
        var user1 = new IdentityUser
        {
            UserName = "user1@kdg.be",
            Email = "user1@kdg.be"
        };
        userManager.CreateAsync(user1, "Password1!").Wait();
        var user2 = new IdentityUser
        {
            UserName = "user2@kdg.be",
            Email = "user2@kdg.be"
        };
        userManager.CreateAsync(user2, "Password1!").Wait();
        
        userManager.AddToRoleAsync(admin1, CustomIdentityConstants.AdminRole).Wait();
        userManager.AddToRoleAsync(admin2, CustomIdentityConstants.AdminRole).Wait();
        userManager.AddToRoleAsync(admin3, CustomIdentityConstants.AdminRole).Wait();
        userManager.AddToRoleAsync(manager1, CustomIdentityConstants.ManagerRole).Wait();
        userManager.AddToRoleAsync(manager2, CustomIdentityConstants.ManagerRole).Wait();
        userManager.AddToRoleAsync(user1, CustomIdentityConstants.UserRole).Wait();
        userManager.AddToRoleAsync(user2, CustomIdentityConstants.UserRole).Wait();
    }
}