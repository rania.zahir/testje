import '@popperjs/core';
import 'bootstrap';
import 'bootstrap-icons/font/bootstrap-icons.css';
import 'bootstrap/dist/css/bootstrap.css';

// Custom CSS imports
import '../scss/site.scss';
import '../scss/dashboard.scss';

console.log('The \'site\' bundle has been loaded!');