﻿// Initialize QR code scanner
let htmlScanner = new Html5QrcodeScanner(
    "my-qr-reader",
    { fps: 800, qrbox: 500 }
);

// Render the QR code scanner and handle scan success
htmlScanner.render(onScanSuccess);

function onScanSuccess(decodedText, decodedResult) {
    console.log(`Code matched = ${decodedText}`, decodedResult);
    selectOptions(decodedText);
}

function selectOptions(decoded) {
    const answerIndex = parseInt(decoded) - 1;
    console.log(`Scanned answer index: ${answerIndex}`);

    if (answerIndex >= 0 && answerIndex < questions[currentQuestionIndex].options.length) {
        const currentQuestion = questions[currentQuestionIndex];
        console.log("Current question:", currentQuestion);

        if (currentQuestion.type === 3) {
            console.log("Handling type 3 question");
            const radioButtons = document.querySelectorAll('input[type="radio"]');
            if (radioButtons.length > answerIndex) {
                radioButtons[answerIndex].checked = true;
            }
        } else if (currentQuestion.type === 1) {
            console.log("Handling type 1 question");
            const inputRange = document.querySelector('.inputRange');
            if (inputRange) {
                inputRange.value = answerIndex.toString();
            }
        } else if (currentQuestion.type === 0) {
            console.log("Handling type 0 question");
            const checkboxes = document.querySelectorAll('input[type="checkbox"]');
            console.log("Checkboxes:", checkboxes);
            checkboxes.forEach((checkbox, index) => {
                console.log("Checkbox value:", checkbox.value);
                console.log("Index:", index);
                console.log("Is index equal to answerIndex?", index === answerIndex);
                checkbox.checked = index === answerIndex;
            });
        }
    } else {
        console.log("Invalid answer index scanned.");
    }
}

