﻿document.addEventListener("DOMContentLoaded", function () {
    loadQuestions();
    
});

let questions = 0;
let currentQuestionIndexLin = 0;

function loadQuestions() {
    const guid = document.getElementById('subthemeId').innerText
    fetch('/api/Subtheme/' + guid.valueOf() + '/questions', {
        method: "GET",
        headers: {
            "Accept": "application/json"
        },
    })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            questions = data;
            displayQuestion()
        })
        .catch(reason => alert("Call failed: " + reason));
}

function saveAnswer() {
    const question = questions[currentQuestionIndexLin];
    const optionsArray = [];
    const answerBody = {
        questionId: question.id,
        options: optionsArray,
        openText: ""
    };

    if (question.type === 3) {
        const selectedOption = document.querySelector('input[type="radio"]:checked');
        if (selectedOption !== null) {
            console.log("Selected radio button value:", selectedOption.value);
            optionsArray.push(selectedOption.value);
        } else {
            console.log("No radio button is checked.");
            if (selectedOption !== null) {
                optionsArray.push(selectedOption.value); // Ensure you push the value, not the element
            }
            answerBody.options = optionsArray;
        }
    } else if (question.type === 2) {
        answerBody.openText = document.querySelector('.inputoptions').value;
    } else if (question.type === 1) {
        const selectedRangeValue = document.querySelector('.inputRange').value;
        answerBody.options = optionsArray.push(question.options[selectedRangeValue].value.toString());
        answerBody.options = optionsArray;
    } else if (question.type === 0) {
        const selectedOptions = document.querySelectorAll('input[type="checkbox"]:checked');
        const arrayOfOptions = Array.from(selectedOptions)
        arrayOfOptions.forEach(value => optionsArray.push(value.value))
        answerBody.options = optionsArray;
    }

    fetch(`/api/Answer/answer`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json"
        },
        body: JSON.stringify(answerBody)
    })
        .then(response => {
            if (response.status !== 200) {
                throw new Error(`Unsupported status code ${response.status}.`);
            }
        })
        .catch(reason => alert("Call failed: " + reason));
}

function linNextQuestion() {
    saveAnswer()
    currentQuestionIndexLin++;
    if (currentQuestionIndexLin < questions.length) {
        displayQuestion();
    } else {
        window.location.href = '/User/userCreation'
    }
}

function linPreviousQuestion() {
    if (currentQuestionIndexLin > 0) {
        currentQuestionIndexLin--;
        displayQuestion();
    }
}

function setProgressBar() {
    console.log("Setting progress bar")
    const progressBar = document.getElementById("pro-bar");
    const progress = (currentQuestionIndexLin + 1) / questions.length * 100;

    if (progressBar) {
        progressBar.style.width = progress + "%";
        progressBar.setAttribute("aria-valuenow", progress.toString())
    }
}

function displayQuestion() {
    var currentQuestionLin = questions[currentQuestionIndexLin];
    var questionContainerLin = document.getElementById("questionLin");
    questionContainerLin.innerHTML = "<br><p class='questionLinFlow'>" + currentQuestionLin.prompt + "</p>";

    setProgressBar();
    startTimer();

    if (currentQuestionLin.type === 3) {
        questionContainerLin.innerHTML += "<div class='form-check'>"
        currentQuestionLin.options.forEach((option, index) => {
            questionContainerLin.innerHTML += "<input class='inputoptions form-check-input' name='radiobuttonquestion' type='radio' value='" + option.value + "'>" +
                "<label class='form-check-label' style='color: " + getOptionColor(index) + "'>" + option.label + "</label><br>";
        });
        questionContainerLin.innerHTML += "</div>"
    } else if (currentQuestionLin.type === 0) {
        currentQuestionLin.options.forEach((option, index) => {
            questionContainerLin.innerHTML += "<input class='inputoptions' type='checkbox' value='" + option.value + "'> <span style='color: " + getOptionColor(index) + "'>" + option.label + "</span><br> ";
        });
    } else if (currentQuestionLin.type === 2) {
        questionContainerLin.innerHTML += "<textarea class='inputoptions' cols='55' rows='5'>";
    } else if (currentQuestionLin.type === 1) {
        //container range balk en opties
        const rangeContainer = document.createElement("div");
        rangeContainer.classList.add("rangeContainer");
        rangeContainer.style.display = "flex"; //flex-container
        rangeContainer.style.flexDirection = "column"; //range balk boven opties
        rangeContainer.style.width = "60%";
        rangeContainer.style.margin = "auto";
        //input range balk
        const inputRange = document.createElement("input");
        inputRange.classList.add("inputRange");
        inputRange.type = "range";
        inputRange.min = '0';
        inputRange.max = (currentQuestionLin.options.length - 1).toString();
        inputRange.step = '1';
        // inputRange.value = currentQuestionLin.value;
        rangeContainer.appendChild(inputRange);

        //opties naast elkaar 
        const rangeQuestionOptions = document.createElement("div");
        rangeQuestionOptions.classList.add("row");
        rangeQuestionOptions.style.display = "flex";
        rangeQuestionOptions.style.flexWrap = "wrap";

        const optionWidth = 100 / currentQuestionLin.options.length; //breedte elke kolom

        currentQuestionLin.options.forEach(((option, index) => {
            const col = document.createElement("div");
            col.classList.add("col");
            col.id = "rangeQuestionOptions";
            col.style.width = optionWidth + "%"; //breedte elke kolom op basis van aantal opties
            col.textContent = option.label;
            col.style.textAlign = "center";
            rangeQuestionOptions.appendChild(col);
        }));

        rangeContainer.appendChild(rangeQuestionOptions);
        questionContainerLin.appendChild(rangeContainer);
    }
}

function getOptionColor(index) {
    const colors = ["red", "#F28C28", "green", "blue", "purple"];
    return colors[index % colors.length];
}
