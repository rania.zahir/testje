﻿var questionsData = [];

function addQuestionToList(prompt, type, options, subthema) {
    var questionData = {
        Prompt: prompt,
        Type: parseInt(type),
        Options: options.map((opt, ind) => ({Label: opt, Value: ind})),
        IsOpen: type === "2",
        SubthemeId: subthema
    };
    questionsData.push(questionData);
    var questionList = document.getElementById("questionList");
    var listItem = document.createElement("li");

    listItem.textContent = "Vraag " + (questionsData.length) + ": " + prompt;

    if (options.length > 0) {
        var optionsText = document.createElement("div");
        optionsText.textContent = "Opties: " + options.join(", ");
        listItem.appendChild(document.createElement("br"));
        listItem.appendChild(optionsText);
    }

    questionList.appendChild(listItem);

    console.log("Verzonden array", questionsData);
}

var optionCounter = 0;

document.getElementById("questionType").addEventListener("change", function () {
    var selectedType = this.value;
    var questionFields = document.getElementById("questionFields");
    questionFields.innerHTML = "";

    if (selectedType === "") {
        questionFields.style.display = "none";
    } else {
        questionFields.style.display = "block";
        if (selectedType === "2") {
            questionFields.innerHTML = `
                            <label for="Content">Vraag:</label><br>
                            <input type="text" id="Content" name="Prompt" class="form-control" required><br>
                        `;
            document.getElementById("finishButton").style.display = "block";
            document.getElementById("addOptionButton").style.display = "none";
            document.getElementById("removeOptionButton").style.display = "none";
        } else if (selectedType === "3") {
            questionFields.innerHTML = `
                            <label for="Content">Vraag:</label><br>
                            <input type="text" id="Content" name="Prompt" class="form-control" required><br>
                            <div id="optionsContainer">
                                <label for="Label">Antwoorden:</label><br>
                               <input type="text" name="Option[${optionCounter}].Label" class="form-control" required><br>
                                <input type="text" name="Option[${optionCounter + 1}].Label" class="form-control" required><br>
                            </div><br>
                        `;
            document.getElementById("addOptionButton").style.display = "block";
            document.getElementById("removeOptionButton").style.display = "block";
        } else if (selectedType === "1") {
            questionFields.innerHTML = `
                            <label for="Content">Vraag:</label><br>
                            <input type="text" id="Content" name="Prompt" class="form-control" required><br>
                            <div id="optionsContainer">
                                <label>Antwoorden:</label><br>
                               <input id="Option" type="text" name="Option[${optionCounter}].Label" class="form-control" required placeholder="laagste waarde"><br>
                               <input id="Option" type="text" name="Option[${optionCounter + 1}].Label" class="form-control" required><br>
                            </div><br>
                        `;
            document.getElementById("addOptionButton").style.display = "block";
            document.getElementById("removeOptionButton").style.display = "block";
        } else if (selectedType === "0") {
            questionFields.innerHTML = `
                            <label for="Content" class="form-label">Vraag:</label><br>
                            <input type="text" id="Content" name="Prompt" class="form-control" required><br>
                            <div id="optionsContainer">
                                <label>Antwoorden:</label><br>
                                   <input type="text" name="Option[${optionCounter}].Label" class="form-control" required><br>
                                   <input type="text" name="Option[${optionCounter + 1}].Label" class="form-control" required<br>
                            </div><br>
                        `;
            document.getElementById("addOptionButton").style.display = "block";
            document.getElementById("removeOptionButton").style.display = "block";
        }
    }
    document.getElementById("addQuestionButton").style.display = "block";
    document.getElementById("finishButton").style.display = "block";
});

document.getElementById("addOptionButton").addEventListener("click", function () {
    var optionsContainer = document.getElementById("optionsContainer");
    var newOption = document.createElement("input");
    optionCounter++;
    newOption.type = "text";
    newOption.name = `Option[${optionCounter}].Label`;
    newOption.required = true;
    newOption.className = "form-control";

    optionsContainer.appendChild(newOption);
    optionsContainer.appendChild(document.createElement("br"));


});
document.getElementsByTagName("input").style = "border: 2px solid black;";
document.getElementById("removeOptionButton").addEventListener("click", function () {
    var optionsContainer = document.getElementById("optionsContainer");
    var optionInputs = optionsContainer.querySelectorAll("input[type=text][name^='Option']");

    //verwijder laatst toegevoegde optie
    if (optionInputs.length > 2) {
        optionsContainer.removeChild(optionInputs[optionInputs.length - 1]); //verwijder het inputveld
        optionsContainer.removeChild(optionsContainer.lastChild); //verwijder bijbehorende <br> tag
    }

});

function addQuestion() {
    var questionType = document.getElementById("questionType").value;
    var questionContent = document.getElementById("Content").value;
    var subthemaId = document.getElementById("SubthemaId").value;
    var options = [];

    var areOptionsValid = true;

    //opties ophalen en valideren
    var optionInputs = document.querySelectorAll("input[name^='Option']");
    optionInputs.forEach(function (input) {
        var optionValue = input.value.trim();
        if (optionValue !== "") {
            options.push(optionValue);
        } else {
            areOptionsValid = false;
        }
    });

    if (questionContent.trim() === "") {
        alert("Vul de vraag in voordat u deze toevoegt aan de lijst.");
    } else if (!areOptionsValid) {
        alert("Vul alle opties in voordat u de vraag toevoegt aan de lijst.");
    } else if (subthemaId.trim() === "") {
        alert("Selecteer een subthema voordat u de vraag toevoegt aan de lijst.");
    } else {
        addQuestionToList(questionContent, questionType, options, subthemaId);
        clearForm();
        document.getElementById("questionCount").textContent = "Aantal vragen: " + questionsData.length;
        sendLastQuestionToDatabase();
    }
}

document.getElementById("addQuestionButton").addEventListener("click", addQuestion);
function sendLastQuestionToDatabase() {
    var lastQuestion = questionsData[questionsData.length - 1];
    console.log(JSON.stringify(lastQuestion));

    fetch("/api/Questions", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(lastQuestion)
    })
        .then(response => {
            if (!response.ok) {
                return response.json().then(error => {
                    throw new Error(error.message || "Er is een fout opgetreden bij het verwerken van uw verzoek.");
                });
            }
            return response.json();
        })
        .then(data => {
            console.log("Data successfully sent to server:", data);
            //reset formulier
            clearForm();
            document.getElementById("questionCount").textContent = "Aantal vragen: " + questionsData.length;
        })
        .catch(error => {
            console.error("Error sending data to server:", error);
            console.log("Data die in de db moet komen:", lastQuestion);
            alert("Er is een fout opgetreden bij het verwerken van uw verzoek.");
        });
}

function clearForm() {
    document.getElementById("Content").value = "";
    var questionType = document.getElementById("questionType").value;

    var optionsContainer = document.getElementById("optionsContainer");

    if (optionsContainer) {
        if (questionType === "2") {
            optionsContainer.innerHTML = '';
        } else {
            optionsContainer.innerHTML = `
                <label>Antwoorden:</label><br>
                <input type="text" name="Option[0].Label" class="form-control" required><br>
                <input type="text" name="Option[1].Label" class="form-control" required><br>
            `;
        }
    }
}

document.getElementById("finishButton").addEventListener("click", function () {
    var isConfirmed = confirm("Bent u zeker dat u wilt doorgaan?");
    if (isConfirmed) {
        addQuestion();
        window.location.href = "/Question/Index";
    }
});
