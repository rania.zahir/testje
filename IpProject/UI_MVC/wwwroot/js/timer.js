function startTimer() {
    let secs = 30;

    function Decrement() {
        if (document.getElementById) {
            let timer = document.getElementById("countdown");
            let minutes = Math.floor(secs / 60);
            let remaingSeconds = secs % 60;

            if (remaingSeconds < 10) {
                timer.innerText = `Timer: ${minutes} min 0${remaingSeconds} sec`;
            } else {
                timer.innerText = `Timer: ${minutes} min ${remaingSeconds} sec`;
            }

            if (secs <= 0) {
                timer.innerText = "Tijd is om";
                linNextQuestion();
                secs = 30;
            } else {
                secs--;
                setTimeout(Decrement, 1000);
            }
        }
    }

    Decrement();
}
