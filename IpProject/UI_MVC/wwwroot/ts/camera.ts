﻿// async function initializeCamera(): Promise<void> {
//     try {
//         // Constraints om alleen de camera te gebruiken
//         const constraints: MediaStreamConstraints = { video: true };
//
//         // Toegang krijgen tot de camera
//         const stream: MediaStream = await navigator.mediaDevices.getUserMedia(constraints);
//
//         // Het videostream instellen als bron voor het video-element
//         const video: HTMLVideoElement | null = document.querySelector("video");
//         if (video) {
//             video.srcObject = stream;
//         } else {
//             console.error("Video element not found.");
//         }
//     } catch (error) {
//         console.error("Error accessing camera:", error);
//     }
// }
//
// // Checken of getUserMedia wordt ondersteund
// if (!navigator.mediaDevices || !navigator.mediaDevices.getUserMedia) {
//     const video: HTMLVideoElement | null = document.querySelector("video");
//     if (video) {
//         video.src = "fallbackvideo.webm";
//     }
//     console.log("getUserMedia wordt niet ondersteund.");
// } else {
//     // Initialiseren van de camera
//     initializeCamera();
// }
