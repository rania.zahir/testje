using IP.BL.API;
using IP.BL.Domain.User;
using IP.DAL.EF;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using UI_MVC.Models;

namespace IpProject.UI.MVC.Controllers;

public class PlatformController : Controller
{
    private readonly IFlowManager _flowManager;
    private readonly UserManager<IdentityUser> _userManager;
    private readonly RoleManager<IdentityRole> _roleManager;

    public PlatformController(AppDbContext context, IFlowManager flowManager, UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager)
    {
        _flowManager = flowManager;
        _userManager = userManager;
        _roleManager = roleManager;
    }
    
    // GET: /Platform
    [HttpGet]
    public IActionResult Index()
    {
        var allPlatforms = _flowManager.GetAllPlatforms();
        return View(allPlatforms);
    }
    
    // GET: /Platform/Create
    public IActionResult Create()
    {
        return View();
    }

    // POST: /Platform/Create
    [HttpPost]
    public IActionResult Create(NewPlatformViewModel model)
    {
        if (!ModelState.IsValid)
        {
            return View(model);
        }
        
        var platform = new Platform
        {
            Name = model.Name
        };
        
        _flowManager.AddPlatform(platform);
        return RedirectToAction("Index", "Platform");
    }
}