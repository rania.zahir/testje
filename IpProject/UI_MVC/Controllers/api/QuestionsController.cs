﻿using IP.BL.API;
using IP.BL.Domain.Flows.FlowComponents;
using IP.BL.Domain.Flows.FlowComponents.Questions;
using IpProject.UI.MVC.Models.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IpProject.UI.MVC.Controllers;

[Route("api/[controller]")]
[ApiController]
[Authorize(Roles = "admin")]
public class QuestionsController : ControllerBase
{
    private readonly IFlowManager _manager;

    public QuestionsController(IFlowManager manager)
    {
        _manager = manager;
    }

    [HttpGet]
    public ActionResult<IEnumerable<Subtheme>> GetSubthemes()
    {
        var subthemes = _manager.GetAllSubthemes();
        if (!subthemes.Any())
        {
            return NoContent();
        }
        return Ok(subthemes);
    }
    
    [HttpPost]
    public IActionResult Add(NewQuestionDto questionDto)
    {
        var question = new Question()
        {
            Id = questionDto.Id,
            Type = questionDto.Type,
            Prompt = questionDto.Prompt,
            Options = questionDto.Options.Select((o) => new Option()
            {
                Label = o.Label,
                Value = o.Value
            }).ToList(),
            IsOpen = questionDto.IsOpen,
            SubthemeId = questionDto.SubthemeId
        };
        
        _manager.AddQuestion(question);
        return Ok(new { message = "Questions successfully added." });
    }
    
}