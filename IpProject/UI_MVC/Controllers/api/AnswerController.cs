using IP.BL.API;
using IP.BL.Domain.Flows.FlowComponents.Questions;
using IP.BL.Managers;
using IpProject.UI.MVC.Models.Dtos;
using Microsoft.AspNetCore.Mvc;

namespace IpProject.UI.MVC.Controllers.api;

[ApiController]
[Route("/api/[controller]")]
public class AnswerController : ControllerBase
{
    private readonly IAnswerManager _answerManager;
    private readonly UnitOfWork _uow;

    public AnswerController(IAnswerManager answerManager, UnitOfWork uow)
    {
        _answerManager = answerManager;
        _uow = uow;
    }

    [HttpPost("answer")]
    public IActionResult SubmitAnswer(AnswerDto answerDto)
    {
        if (!ModelState.IsValid)
        {
            return BadRequest(ModelState);
        }

        _uow.BeginTransaction();
        _answerManager.AddAnswerToQuestion(
            new Answer
            {
                Id = Guid.NewGuid(),
                Question = null,
                Options = null,
                OpenText = answerDto.OpenText
            }
        , answerDto.QuestionId,
            answerDto.Options
            );
        _uow.Commit();

        return Ok();
    }
}

