﻿
using IP.BL.API;
using IP.BL.Domain.Flows.FlowComponents;
using IP.BL.Domain.Flows.FlowComponents.Questions;
using IP.BL.Managers;
using IpProject.UI.MVC.Models.Dtos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using UI_MVC.Models;

namespace IpProject.UI.MVC.Controllers.api;

[ApiController]
[Route("/api/[controller]")]
public class FlowsController : ControllerBase
{
    private readonly IFlowManager _flowManager;
    private readonly IAnswerManager _answerManager;
    private readonly UnitOfWork _uow;

    public FlowsController(IFlowManager flowManager, IAnswerManager answerManager, UnitOfWork unitOfWork)
    {
        _flowManager = flowManager;
        _answerManager = answerManager;
        _uow = unitOfWork;
    }
    
}