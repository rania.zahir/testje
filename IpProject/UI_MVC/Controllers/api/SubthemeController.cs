using IP.BL.API;
using IP.BL.Managers;
using IpProject.UI.MVC.Models.Dtos;
using Microsoft.AspNetCore.Mvc;

namespace IpProject.UI.MVC.Controllers.api;

[ApiController]
[Route("/api/[controller]")]
public class SubthemeController : ControllerBase
{
    private readonly UnitOfWork _uow;
    private readonly ISubthemeManager _subthemeManager;

    public SubthemeController(UnitOfWork uow, ISubthemeManager subthemeManager)
    {
        _uow = uow;
        _subthemeManager = subthemeManager;
    }

    [HttpGet("{subthemeId}/questions")]
    public IActionResult GetQuestions(string subthemeId)
    {
        try
        {
            var mode = ModeManager.GetCurrentMode();
            _uow.BeginTransaction();
            var questions = _subthemeManager.GetQuestionsWithOptions(Guid.Parse(subthemeId), mode);
            _uow.Commit();

            if (questions == null || !questions.Any())
                return NoContent();

            var questionDtos = questions
                .Select(item => new QuestionDto()
                {
                    Id = item.Id,
                    Type = item.Type,
                    Prompt = item.Prompt,
                    Options = item.Options.Select(option => new OptionDto
                    {
                        Id = option.Id,
                        Value = option.Value,
                        Label = option.Label

                    }).ToList()
                })
                .ToList();


            return Ok(questionDtos);
        }
        catch (Exception ex)
        {
            return StatusCode(500, $"Internal server error: {ex.Message}");
        }
    }
}