using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IpProject.UI.MVC.Controllers;

[Authorize(Roles = "admin, manager")] // Restrict access to admins and managers
public class DashboardBeheerder : Controller
{
    // GET
    public IActionResult Index()
    {
        return View();
    }
}