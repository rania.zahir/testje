﻿using IP.BL.API;
using IP.BL.Managers;
using Microsoft.AspNetCore.Mvc;
using UI_MVC.Models;

namespace IpProject.UI.MVC.Controllers;

public class LineFlowController : Controller
{
    private readonly IFlowManager _flowManager;
    private readonly ISubthemeManager _subthemeManager;

    public LineFlowController(IFlowManager flowManager, ISubthemeManager subthemeManager)
    {
        _flowManager = flowManager;
        _subthemeManager = subthemeManager;
    }

    [HttpGet]
    public IActionResult Index()
    {
        var model = _flowManager.GetAllSubthemes();
        return View(model);
    }
    
    [HttpGet]
    public IActionResult Questions(Guid subthemeId)
    {
        var subtheme = _subthemeManager.GetSubtheme(subthemeId);
        var model = new SubthemeFlowViewModel
        {
            FlowItems = subtheme.FlowItems,
            Description = subtheme.Description,
            Id = subtheme.Id,
            Name = subtheme.Name,
            ModeString = ModeManager.GetCurrentMode()
        };
        return View(model);
    }
    
    [HttpGet]
    public IActionResult ThankPage()
    {
        return View();
    }
    
    
}