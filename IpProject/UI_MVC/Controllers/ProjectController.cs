using IP.BL.API;
using IP.BL.Domain;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using UI_MVC.Models;

namespace IpProject.UI.MVC.Controllers;

public class ProjectController : Controller
{
    private readonly IFlowManager _flowManager;
    private readonly UserManager<IdentityUser> _userManager;
    private readonly RoleManager<IdentityRole> _roleManager;

    public ProjectController(IFlowManager flowManager, UserManager<IdentityUser> userManager,
        RoleManager<IdentityRole> roleManager)
    {
        _flowManager = flowManager;
        _userManager = userManager;
        _roleManager = roleManager;
    }

    [HttpGet]
    public IActionResult Index()
    {
        var allPlatformWithProjects = _flowManager.GetAllProjectsWithPlatform();
        return View(allPlatformWithProjects);
    }

    [HttpGet]
    public IActionResult Add()
    {
        var platforms = _flowManager.GetAllPlatforms();
        var viewModel = new NewProjectViewModel
        {
            Platforms = platforms
                .Select(p => new SelectListItem
                {
                    Value = p.Id.ToString(),
                    Text = p.Name
                }).ToList()
        };
        return View(viewModel);
    }

    [HttpPost]
    public IActionResult Add(NewProjectViewModel model)
    {
        if (!ModelState.IsValid)
        {
            return View(model);
        }
        
        var project = new Project
        {
            Title = model.Title,
            Description = model.Description,
            PlatformId = model.PlatformId
        };

        _flowManager.AddProject(project);

        return RedirectToAction("Index", "Project"); // Redirect to platform index page or wherever you need to go
    }
}