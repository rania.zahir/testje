using IP.BL.API;
using Microsoft.AspNetCore.Mvc;

namespace IpProject.UI.MVC.Controllers;

public class DashboardAdmin : Controller
{
    private readonly IFlowManager _flowManager;
    private readonly IUserManager _userManager;

    public DashboardAdmin(IFlowManager flowManager,IUserManager userManager)
    {
        _flowManager = flowManager;
        _userManager = userManager;
    }

    public IActionResult Index()
    {
        var platforms = _flowManager.GetAllPlatforms();
        return View(platforms);
    }
    public IActionResult CreatePlatform()
    {
        return RedirectToAction("Create", "Platform");
    }
}