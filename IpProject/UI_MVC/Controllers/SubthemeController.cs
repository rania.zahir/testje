using IP.BL.API;
using IP.BL.Domain.Flows.FlowComponents;
using IP.BL.Domain.Flows.FlowComponents.Questions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using UI_MVC.Models;

namespace IpProject.UI.MVC.Controllers;

public class SubthemeController : Controller
{
    private readonly ISubthemeManager _subthemeManager;
    private readonly IFlowManager _flowManager;

    public SubthemeController(ISubthemeManager subthemeManager, IFlowManager flowManager)
    {
        _subthemeManager = subthemeManager;
        _flowManager = flowManager;
    }
    
    // GET
    [HttpGet]
    public IActionResult Index()
    {
        var subthemes = _subthemeManager.GetAllSubthemes();
        return View(subthemes);
    }

    [HttpGet]
    public IActionResult AddSubtheme()
    {
        var flows = _flowManager.GetAllFlows();
        var viewModel = new NewSubthemeViewModel
        {
             Flows = flows
                .Select(p => new SelectListItem
                {
                    Value = p.Id.ToString(),
                    Text = p.Name
                }).ToList()
        };
        return View(viewModel);
    }

    [HttpPost]
    public IActionResult AddSubtheme(NewSubthemeViewModel model)
    {
        if (!ModelState.IsValid)
        {
            return View(model);
        }
        
        var subtheme = new Subtheme
        {
            Name = model.Name,
            Description = model.Description,
            FlowId = model.FlowId
        };

        _subthemeManager.AddSubtheme(subtheme);
        return RedirectToAction("Add", "Question");
    }
    
    
    
}