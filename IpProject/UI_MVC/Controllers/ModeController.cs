﻿using IP.BL.Managers;
using Microsoft.AspNetCore.Mvc;

namespace IpProject.UI.MVC.Controllers;

public class ModeController : Controller
{
    [HttpGet]
    public IActionResult Index()
    {
        return View();
    }
    
    [HttpPost]
    public IActionResult UpdateConfig()
    {
        ModeManager.SetCurrentMode(Request.Form.ContainsKey("phygital") ? "phygital" : "kiosk");
        return View("Index");
    }
}