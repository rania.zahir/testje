using IP.BL.API;
using IP.BL.Managers;
using IpProject.UI.MVC.Models.User;
using Microsoft.AspNetCore.Mvc;

namespace IpProject.UI.MVC.Controllers.User;

public class UserController : Controller
{
    private readonly IUserManager _userManager;
    private readonly UnitOfWork _unitOfWork;

    public UserController(IUserManager userManager, UnitOfWork unitOfWork)
    {
        _userManager = userManager;
        _unitOfWork = unitOfWork;
    }

    [HttpGet]
    public IActionResult UserCreation()
    {
        return View();
    }
    
    [HttpPost]
    public IActionResult UserCreation(UserModel userModel)
    {
        if (ModelState.IsValid)
        {
            _unitOfWork.BeginTransaction();
            _userManager.AddUser(userModel.Email);
            _unitOfWork.Commit();
        }

        return View(userModel);
    }

    [HttpGet]
    public IActionResult AddToPlatform(Guid platformId)
    {
        var platform = _userManager.GetPlatform(platformId);
        if (platform == null)
        {
            return NotFound();
        }
        return RedirectToAction("Index", "Platform");
    }
}