using IP.BL.API;
using IP.BL.Domain.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using UI_MVC.Models;

namespace IpProject.UI.MVC.Controllers;

[Authorize(Roles = "admin")]
public class AdminController : Controller
{
    private readonly UserManager<IdentityUser> _userManager;
    private readonly RoleManager<IdentityRole> _roleManager;
    private readonly IFlowManager _flowManager;

    public AdminController(UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager,
        IFlowManager flowManager)
    {
        _userManager = userManager;
        _roleManager = roleManager;
        _flowManager = flowManager;
    }

    [HttpGet]
    public IActionResult Index()
    {
        return View();
    }


    [HttpGet]
    public async Task<IActionResult> Overview()
    {
        var users = await _userManager.Users.ToListAsync();
        var usersWithRoles = new List<UserWithRolesViewModel>();

        foreach (var user in users)
        {
            var roles = await _userManager.GetRolesAsync(user);
            
            var userWithRoles = new UserWithRolesViewModel
            {
                User = user,
                Roles = roles
            };
            usersWithRoles.Add(userWithRoles);
        }

        return View(usersWithRoles);
    }
    

    [HttpGet]
    public IActionResult AddUser()
    {
        var platforms = _flowManager.GetAllPlatforms();
        var model = new NewUserViewModel
        {
            AvailableRoles = CustomIdentityConstants.GetRoleNames(),
            Platforms = platforms
                .Select(p => new SelectListItem
                {
                    Value = p.Id.ToString(),
                    Text = p.Name
                }).ToList()
        };
        return View(model);
    }

    [HttpPost]
    public async Task<IActionResult> AddUser(NewUserViewModel model)
    {
        if (!ModelState.IsValid)
        {
            model.AvailableRoles = CustomIdentityConstants.GetRoleNames();
            model.Platforms = _flowManager.GetAllPlatforms()
                .Select(p => new SelectListItem
                {
                    Value = p.Id.ToString(),
                    Text = p.Name
                }).ToList();
            return View(model);
        }

        var existingUserByName = await _userManager.FindByNameAsync(model.UserName);
        if (existingUserByName != null)
        {
            ModelState.AddModelError(string.Empty, "Gebruiker met deze gebruikernaam bestaat al.");
            model.AvailableRoles = CustomIdentityConstants.GetRoleNames();
            model.Platforms = _flowManager.GetAllPlatforms()
                .Select(p => new SelectListItem
                {
                    Value = p.Id.ToString(),
                    Text = p.Name
                }).ToList();
            return View(model);
        }

        var existingUserByEmail = await _userManager.FindByEmailAsync(model.Email);
        if (existingUserByEmail != null)
        {
            ModelState.AddModelError(string.Empty, "Gebruiker met deze email bestaat al.");
            model.AvailableRoles = CustomIdentityConstants.GetRoleNames();
            model.Platforms = _flowManager.GetAllPlatforms()
                .Select(p => new SelectListItem
                {
                    Value = p.Id.ToString(),
                    Text = p.Name
                }).ToList();
            return View(model);
        }


        var user = new IdentityUser { UserName = model.UserName, Email = model.Email };
        var result = await _userManager.CreateAsync(user, model.Password);

        if (result.Succeeded)
        {
            if (!string.IsNullOrEmpty(model.SelectedRole))
            {
                // Controleren of de huidige gebruiker een manager is
                var isManager = User.IsInRole(CustomIdentityConstants.ManagerRole);

                // Manager mag alleen manager- en gebruikersrollen toewijzen
                if (isManager && (model.SelectedRole != CustomIdentityConstants.ManagerRole &&
                                  model.SelectedRole != CustomIdentityConstants.UserRole))
                {
                    ModelState.AddModelError(string.Empty, "Alleen manager- en gebruikersrollen zijn toegestaan.");
                    model.AvailableRoles = CustomIdentityConstants.GetRoleNamesForManager();
                    model.Platforms = _flowManager.GetAllPlatforms()
                        .Select(p => new SelectListItem
                        {
                            Value = p.Id.ToString(),
                            Text = p.Name
                        }).ToList();
                    return View(model);
                }

                if (await _roleManager.RoleExistsAsync(model.SelectedRole))
                {
                    await _userManager.AddToRoleAsync(user, model.SelectedRole);
                }
                else
                {
                    model.AvailableRoles = CustomIdentityConstants.GetRoleNames();
                    model.Platforms = _flowManager.GetAllPlatforms()
                        .Select(p => new SelectListItem
                        {
                            Value = p.Id.ToString(),
                            Text = p.Name
                        }).ToList();
                    return View(model);
                }
            }

            return RedirectToAction("Overview", "Admin");
        }

        return View(model);
    }

    [HttpGet]
    public async Task<IActionResult> Update(string id)
    {
        var user = await _userManager.FindByIdAsync(id);
        if (user == null)
        {
            return NotFound();
        }

        return View(user);
    }

    [HttpPost]
    public async Task<IActionResult> Update(string id, IdentityUser model)
    {
        if (id != model.Id)
        {
            return NotFound();
        }

        var existingUserByName = await _userManager.FindByNameAsync(model.UserName);
        if (existingUserByName != null && existingUserByName.Id != id)
        {
            ModelState.AddModelError(string.Empty, "Updaten niet gelukt, gebruiker met deze gebruikernaam bestaat al.");
            return View(model);
        }

        var existingUserByEmail = await _userManager.FindByEmailAsync(model.Email);
        if (existingUserByEmail != null && existingUserByEmail.Id != id)
        {
            ModelState.AddModelError(string.Empty, "Updaten niet gelukt, gebruiker met deze email bestaat al.");
            return View(model);
        }

        if (!ModelState.IsValid)
        {
            return View(model);
        }

        var user = await _userManager.FindByIdAsync(id);
        user.UserName = model.UserName;
        user.Email = model.Email;
        var result = await _userManager.UpdateAsync(user);

        if (result.Succeeded)
        {
            return RedirectToAction("Overview", "Admin");
        }

        return View(model);
    }

    [HttpGet]
    public async Task<IActionResult> Delete(string id)
    {
        var user = await _userManager.FindByIdAsync(id);
        if (user == null)
        {
            return NotFound();
        }

        return View(user);
    }

    [HttpPost]
    public async Task<IActionResult> Delete(string id, IdentityUser model)
    {
        if (id != model.Id)
        {
            return NotFound();
        }

        var user = await _userManager.FindByIdAsync(id);
        var result = await _userManager.DeleteAsync(user);
        if (result.Succeeded)
        {
            return RedirectToAction("Overview", "Admin");
        }

        return View(model);
    }
}