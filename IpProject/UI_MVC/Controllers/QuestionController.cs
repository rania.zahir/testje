﻿using IP.BL.API;
using IP.BL.Domain.Flows.FlowComponents.Questions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using UI_MVC.Models;

namespace IpProject.UI.MVC.Controllers;

[Authorize(Roles = "admin")]
public class QuestionController : Controller
{
    private readonly IFlowManager _flowManager;

    public QuestionController(IFlowManager flowManager)
    {
        _flowManager = flowManager;
    }

    // GET
    public IActionResult Index()
    {
        var allQuestions = _flowManager.GetAllQuestionsWithOptions();
        return View(allQuestions);
    }

    [HttpGet]
    public IActionResult Add(Guid subthemaId)
    {
        var subthemas = _flowManager.GetAllSubthemes();
        var viewModel = new QuestionViewModel
        {
            SubthemaId = subthemaId,
            Subthema = subthemas
                .Select(p => new SelectListItem
                {
                    Value = p.Id.ToString(),
                    Text = p.Name
                }).ToList()
        };
        return View(viewModel);
    }

    [HttpPost]
    public IActionResult Add(QuestionViewModel questions)
    {
        if (!ModelState.IsValid)
        {
            return View();
        }

        var r = new Question()
        {
            Id = questions.Id,
            Type = questions.Type,
            Prompt = questions.Prompt,
            Options = questions.Option?.Select((o, ind) => new Option()
            {
                Label = o.Label,
                Value = ind
            }).ToList(),
            IsOpen = questions.Type == QuestionType.Open,
            SubthemeId = questions.SubthemaId
        };

        _flowManager.AddQuestion(r);
        return RedirectToAction("Index", "Question");
    }
    
    [HttpPost]
    [Route("/Question/Delete/{id}")]
    public IActionResult Delete(Guid id)
    {
        var question = _flowManager.GetQuestionById(id);
        if (question == null)
        {
            return NotFound();
        }

        _flowManager.RemoveQuestion(question);
        return RedirectToAction(nameof(Index));
    }
}