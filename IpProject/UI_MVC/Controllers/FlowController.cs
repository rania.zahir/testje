﻿using IP.BL.API;
using IP.BL.Domain.Flows;
using IP.BL.Domain.Flows.FlowComponents;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using UI_MVC.Models;

namespace IpProject.UI.MVC.Controllers;

[Authorize(Roles = "admin")]
public class FlowController : Controller
{
    private readonly IFlowManager _flowManager;
    private readonly ISubthemeManager _subthemeManager;

    public FlowController(IFlowManager flowManager, ISubthemeManager subthemeManager)
    {
        _flowManager = flowManager;
        _subthemeManager = subthemeManager;
    }
    
    [HttpGet]
    public IActionResult Create(Guid projectId)
    {
        var viewModel = new NewFlowViewModel
        {
            ProjectId = projectId
        };
        return View(viewModel);
    }


    [HttpPost]
    public IActionResult Create(NewFlowViewModel flowViewModel)
    {
        if (!ModelState.IsValid)
        {
            return View(flowViewModel);
        }

        var flow = new Flow
        {
            Name = flowViewModel.Name,
            Type = flowViewModel.Type,
            Description = flowViewModel.Description,
            ProjectId = flowViewModel.ProjectId
        };

        _flowManager.AddFlow(flow);

        return RedirectToAction("AddSubtheme", "Subtheme");

    }
}