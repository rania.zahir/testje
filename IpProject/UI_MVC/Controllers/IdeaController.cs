﻿using IP.BL.API;
using IP.BL.Managers;
using IP.DAL.API;
using IpProject.UI.MVC.Models.Dtos;
using Microsoft.AspNetCore.Mvc;

namespace IpProject.UI.MVC.Controllers;

public class IdeaController : Controller
{
    private readonly IIdeasManager _ideasManager;
    private readonly UnitOfWork _unitOfWork;

    public IdeaController(IIdeasManager ideasManager, UnitOfWork unitOfWork)
    {
        _ideasManager = ideasManager;
        _unitOfWork = unitOfWork;
    }
    
    [HttpGet]
    public IActionResult IdeaView()
    {
        _unitOfWork.BeginTransaction();
        var ideas = _ideasManager.GetIdeas();
        _unitOfWork.Commit();
        
        var ideaDto = new IdeaDto()
        {
            Ideas = ideas
        };
        
        return View(ideaDto);
    }
}