namespace IpProject.UI.MVC;

public static class CustomIdentityConstants
{
    public const string AdminRole = "admin";
    public const string ManagerRole = "manager";
    public const string UserRole = "user";
    
    public static List<string> GetRoleNames()
    {
        return new List<string> { AdminRole, UserRole, ManagerRole};
    }
    
    public static List<string> GetRoleNamesForManager()
    {
        return new List<string> {UserRole, ManagerRole};
    }
    
}