﻿using IP.BL.Domain.Flows.FlowComponents.Questions;

namespace IP.BL.API;

public interface IAnswerManager
{
    void AddAnswer(Answer answer);
    void AddAnswerToQuestion(Answer answer, string questionId, List<string> options);
}