﻿using IP.BL.Domain.Ideas;

namespace IP.BL.API;

public interface IIdeasManager
{
    Idea GetIdea(Guid ideaId);
    IEnumerable<Idea> GetIdeas();
}