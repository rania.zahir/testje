﻿using IP.BL.Domain;
using IP.BL.Domain.Flows;
using IP.BL.Domain.Flows.FlowComponents;
using IP.BL.Domain.Flows.FlowComponents.Questions;
using IP.BL.Domain.User;

namespace IP.BL.API;

public interface IFlowManager
{
    FlowItem GetNextFlowItem(Guid flowId);
    IEnumerable<Question> GetQuestionsWithOptions(Guid flowId);
    Flow GetFlow(Guid flowId);
    FlowItem GetNextFlowItem(Guid flowId,Guid subthemeId);
    Question GetQuestion(Guid questionId);
    Question GetQuestionWithOptions(Guid questionId);
    void AddFlow(Flow flow);
    Flow GetFlowWithSubthemes(Guid flowId);
    void AddQuestion(Question question);
    IEnumerable<Question> GetAllQuestions();
    IEnumerable<Question> GetAllQuestionsWithOptions();
    void RemoveQuestion(Question  question);
    Question GetQuestionById(Guid id);

    IEnumerable<Project> GetAllProjectsWithPlatform();
    IEnumerable<Platform> GetAllPlatforms();
    void AddProject(Project project);
    IEnumerable<Flow> GetAllFlows();
    IEnumerable<Subtheme> GetAllSubthemes();
    void AddPlatform(Platform platform);
    IEnumerable<Subtheme> GetSubthemes(Guid flowId);
}