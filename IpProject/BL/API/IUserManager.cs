﻿using IP.BL.Domain.User;

namespace IP.BL.API;

public interface IUserManager
{
    void AddUser(string email);
    Platform GetPlatform(Guid platformId);
    
}