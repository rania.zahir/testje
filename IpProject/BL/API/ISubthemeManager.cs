using IP.BL.Domain.Flows.FlowComponents;
using IP.BL.Domain.Flows.FlowComponents.Questions;

namespace IP.BL.API;

public interface ISubthemeManager
{
    FlowItem GetNextFlowItem(Guid flowId);
    IEnumerable<Question> GetQuestionsWithOptions(Guid subthemeId, string mode);
    Subtheme GetSubtheme(Guid subthemeId);
    void AddSubtheme(Subtheme subtheme);
    IEnumerable<Subtheme> GetAllSubthemes();
    void ConnectSubthemeToFlow(Guid flowId, Guid subthemeId);
}