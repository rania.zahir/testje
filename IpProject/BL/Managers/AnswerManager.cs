﻿using IP.BL.API;
using IP.BL.Domain.Flows.FlowComponents.Questions;
using IP.DAL.API;

namespace IP.BL.Managers;

public class AnswerManager : IAnswerManager
{
    private readonly IAnswerRepository _answerRepository;
    private readonly IQuestionRepository _questionRepository;

    public AnswerManager(IAnswerRepository answerRepository, IQuestionRepository questionRepository)
    {
        _answerRepository = answerRepository;
        _questionRepository = questionRepository;
    }

    public void AddAnswer(Answer answer)
    {
        _answerRepository.CreateAnswer(answer);
    }

    public void AddAnswerToQuestion(Answer answer, string questionId, List<string> options)
    {
        var parsedOptions = options.Select(int.Parse).ToList();
        var question = _questionRepository.ReadQuestionWithOptions(Guid.Parse(questionId));
        answer.Question = question;
        answer.Options = question.Options.FindAll(option => parsedOptions.Contains(option.Value));
        _questionRepository.UpdateOptions(answer.Options);
        _answerRepository.CreateAnswer(answer);
    }
}