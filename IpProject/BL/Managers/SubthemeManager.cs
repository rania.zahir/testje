using IP.BL.API;
using IP.BL.Domain.Flows.FlowComponents;
using IP.BL.Domain.Flows.FlowComponents.Questions;
using IP.DAL.API;

namespace IP.BL.Managers;

public class SubthemeManager : ISubthemeManager
{
    private readonly ISubthemeRepository _subthemeRepository;

    public SubthemeManager(ISubthemeRepository subthemeRepository)
    {
        _subthemeRepository = subthemeRepository;
    }

    public FlowItem GetNextFlowItem(Guid flowId)
    {
        throw new NotImplementedException();
    }

    public IEnumerable<Question> GetQuestionsWithOptions(Guid subthemeId, string mode)
    {
        return _subthemeRepository.ReadQuestionsWithOptions(subthemeId, mode);
    }

    public Subtheme GetSubtheme(Guid subthemeId)
    {
        return _subthemeRepository.ReadSubtheme(subthemeId);
    }

    public void AddSubtheme(Subtheme subtheme)
    {
        _subthemeRepository.CreateSubtheme(subtheme);
    }

    public IEnumerable<Subtheme> GetAllSubthemes()
    {
        return _subthemeRepository.ReadAllSubthemes();
    }

    public void ConnectSubthemeToFlow(Guid flowId, Guid subthemeId)
    {
        _subthemeRepository.ConnectSubthemeToFlow(flowId, subthemeId);
    }
}