﻿using IP.DAL.Repositories;

namespace IP.BL.Managers;

public static class ModeManager
{
    public static string GetCurrentMode()
    {
        return ConfigRepository.LoadMode();
    }

    public static void SetCurrentMode(string mode)
    {
        ConfigRepository.SaveMode(mode);
    }
}