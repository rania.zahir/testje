﻿using IP.DAL.EF;
using Microsoft.EntityFrameworkCore.Storage;

namespace IP.BL.Managers;

public class UnitOfWork
{
    private readonly AppDbContext _dbContext;
    private IDbContextTransaction _transaction;


    public UnitOfWork(AppDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    /*
     * BeginTransaction() moet in de controller worden aangeroepen
     * wannneer je een transactie wil starten.
     */
    public void BeginTransaction()
    {
        _transaction = _dbContext.Database.BeginTransaction();
    }


    /*
     * Commit() moet in de controller worden aangeroepen wannneer je
     * een transactie wil eindigen, zodat de wijzigingen in de database
     * worden doorgevoerd.
     */
    public void Commit()
    {
        _dbContext.SaveChanges();
        _dbContext.Database.CommitTransaction();
    }
    
    /*
     * Rollback() wordt gebruikt om de transactie ongedaan te maken
     * in geval van een fout.
     */
    public void Rollback()
    {
        _transaction?.Rollback();
    }
}