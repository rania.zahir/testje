﻿using IP.BL.API;
using IP.BL.Domain.User;
using IP.DAL.API;

namespace IP.BL.Managers;

public class UserManager: IUserManager
{
    private readonly IUserRepository _userRepository;

    public UserManager(IUserRepository userRepository)
    {
        _userRepository = userRepository;
    }

    public void AddUser(string email)
    {
        var user = new User(email);
        _userRepository.Create(user);
    }
    
    public Platform GetPlatform(Guid platformId)
    {
        return _userRepository.ReadPlatform(platformId);
    }
    
}