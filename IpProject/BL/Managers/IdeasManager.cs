﻿using IP.BL.API;
using IP.BL.Domain.Ideas;
using IP.DAL.API;

namespace IP.BL.Managers;

public class IdeasManager : IIdeasManager
{
    private readonly IIdeasRepository _ideasRepository;

    public IdeasManager(IIdeasRepository ideasRepository)
    {
        _ideasRepository = ideasRepository;
    }
    
    public Idea GetIdea(Guid ideaId)
    {
        return _ideasRepository.ReadIdea(ideaId);
    }
    
    public IEnumerable<Idea> GetIdeas()
    {
        return _ideasRepository.ReadIdeas();
    }
    
    
}