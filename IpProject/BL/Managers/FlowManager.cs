﻿
using IP.BL.API;
using IP.BL.Domain;
using IP.BL.Domain.Flows;
using IP.BL.Domain.Flows.FlowComponents;
using IP.BL.Domain.Flows.FlowComponents.Questions;
using IP.BL.Domain.User;
using IP.DAL.API;
using Microsoft.AspNetCore.Identity;

namespace IP.BL.Managers;

public class FlowManager : IFlowManager
{
    private readonly IFlowRepository _flowRepository;


    public FlowManager(IFlowRepository flowRepository)
    {
        _flowRepository = flowRepository;
    }

    public FlowItem GetNextFlowItem(Guid flowId)
    {
        // return _flowRepository.ReadNextFlowItem(flowId);
        throw new NotImplementedException();
    }

    public IEnumerable<Question> GetQuestionsWithOptions(Guid flowId)
    {
        // return _flowRepository.ReadQuestionsWithOptions(flowId);
        throw new NotImplementedException();

    }

    public Flow GetFlow(Guid flowId)
    {
        return _flowRepository.ReadFlow(flowId);
    }

    public FlowItem GetNextFlowItem(Guid flowId, Guid subthemeId)
    {
        throw new NotImplementedException();
    }

    public Question GetQuestion(Guid questionId)
    {
        // return _flowRepository.ReadQuestion(questionId);
        throw new NotImplementedException();

    }

    public Question GetQuestionWithOptions(Guid questionId)
    {
        // return _flowRepository.ReadQuestionWithOptions(questionId);
        throw new NotImplementedException();

    }

    public void AddFlow(Flow flow)
    {
        _flowRepository.CreateFlow(flow);
    }

    public Flow GetFlowWithSubthemes(Guid flowId)
    {
        return _flowRepository.ReadFlowWithSubthemes(flowId);
    }
    
    

    public void AddQuestion(Question question)
    {
        _flowRepository.CreateQuestion(question);
    }

    public IEnumerable<Question> GetAllQuestions()
    {
        return _flowRepository.ReadAllQuestions();
    }

    public IEnumerable<Question> GetAllQuestionsWithOptions()
    {
        return _flowRepository.ReadAllQuestionsWithOptions();
    }

    public void RemoveQuestion(Question question)
    {
        _flowRepository.DeleteQuestion(question);
    }

    public Question GetQuestionById(Guid id)
    {
       return _flowRepository.ReadQuestionById(id);
    }

    public IEnumerable<Project> GetAllProjectsWithPlatform()
    {
        return _flowRepository.ReadAllProjectsWithPlatform();
    }

    public IEnumerable<Platform> GetAllPlatforms()
    {
        return _flowRepository.ReadAllPlatforms();
    }
    
    public void AddProject(Project project)
    {
        _flowRepository.CreateProject(project);
    }

    public IEnumerable<Flow> GetAllFlows()
    {
        return _flowRepository.ReadAllFlows();
    }

    public IEnumerable<Subtheme> GetAllSubthemes()
    {
        return _flowRepository.ReadAllSubthemes();
    }

    public void AddPlatform(Platform platform)
    {
        _flowRepository.CreatePlatform(platform);
    }

    public IEnumerable<Subtheme> GetSubthemes(Guid flowId)
    {
        return _flowRepository.ReadSubthemes(flowId);
    }
}