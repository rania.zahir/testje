﻿using System.Diagnostics;
using IP.BL.Domain;
using IP.BL.Domain.Flows;
using IP.BL.Domain.Flows.FlowComponents;
using IP.BL.Domain.Flows.FlowComponents.Questions;
using IP.BL.Domain.Ideas;
using IP.BL.Domain.User;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace IP.DAL.EF;

using Microsoft.EntityFrameworkCore;

public class AppDbContext : IdentityDbContext<IdentityUser>
{
    public DbSet<FlowItem> FlowItems { get; set; }
    public DbSet<Option> Options { get; set; }
    public DbSet<Answer> Answers { get; set; }
    public DbSet<Flow> Flows { get; set; }
    public DbSet<User> Users { get; set; }
    public DbSet<Subtheme> Subthemes { get; set; }
    public DbSet<Idea> Ideas { get; set; }
    public DbSet<Question> Questions { get; set; }
    public DbSet<Project> Projects { get; set; }
    public DbSet<Platform> Platforms { get; set; }
    
    public DbSet<InfoComponent> InfoComponents { get; set; }
    public AppDbContext(DbContextOptions options) : base(options)
    {
    }
    
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        if (!optionsBuilder.IsConfigured)
        {
            optionsBuilder.UseNpgsql("Server=127.0.0.1;Port=5432;Database=DbApp;User Id=postgres;Password=password;");
        }

        optionsBuilder.LogTo(message =>
            Debug.WriteLine(message), LogLevel.Information);
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        modelBuilder.Entity<Option>()
            .ToTable("Options")
            .HasKey(e => e.Id);
        modelBuilder.Entity<Answer>()
            .ToTable("Answers")
            .HasKey(e => e.Id);
        modelBuilder.Entity<Subtheme>()
            .ToTable("Subtheme")
            .HasKey(e => e.Id);
        modelBuilder.Entity<Flow>()
            .ToTable("Flow")
            .HasKey(e => e.Id);
        modelBuilder.Entity<FlowItem>()
            .ToTable("FlowItem")
            .HasKey(e => e.Id);
        modelBuilder.Entity<Platform>()
            .ToTable("Platform")
            .HasKey(e => e.Id);
        modelBuilder.Entity<User>()
            .ToTable("User")
            .HasKey(e => e.Id);
        modelBuilder.Entity<Idea>()
            .ToTable("Idea")
            .HasKey(e => e.Id);
        modelBuilder.Entity<Project>()
            .ToTable("Project")
            .HasKey(p => p.Id);
        modelBuilder.Entity<Question>()
            .HasBaseType<FlowItem>();
        modelBuilder.Entity<Question>()
            .HasMany(q => q.Options);
        modelBuilder.Entity<Platform>()
            .HasMany(p => p.Projects)
            .WithOne(p => p.Platform)
            .HasForeignKey(p => p.PlatformId);

        modelBuilder.Entity<Project>()
            .HasMany(p => p.Flows)
            .WithOne(f => f.Project)
            .HasForeignKey(f => f.ProjectId);

        modelBuilder.Entity<Flow>()
            .HasMany(f => f.Subthemes)
            .WithOne(s => s.Flow)
            .HasForeignKey(s => s.FlowId);
        
        modelBuilder.Entity<Subtheme>()
            .HasMany(p => p.FlowItems)
            .WithOne(f => f.Subtheme)
            .HasForeignKey(f => f.SubthemeId);
        
        modelBuilder.Entity<InfoComponent>()
            .ToTable("InfoComponent");
        
        // Configure relationship between Platform and Flow
        //modelBuilder.Entity<Platform>()
        //    .HasMany(p => p.Flows);
        //.WithOne(f => f.Platform)
        //.HasForeignKey(f => f.PlatformId)
        //.OnDelete(DeleteBehavior.Cascade); // Cascade delete when platform is deleted
    }

    public bool CreateDatabase(bool dropDatabase)
    {
        if (dropDatabase)
        {
            Database.EnsureDeleted();
        }

        return Database.EnsureCreated();
    }
}