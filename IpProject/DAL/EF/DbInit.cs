using IP.BL.Domain;
using IP.BL.Domain.Flows;
using IP.BL.Domain.Flows.FlowComponents;
using IP.BL.Domain.Flows.FlowComponents.Questions;
using IP.BL.Domain.Ideas;
using IP.BL.Domain.User;
using Microsoft.AspNetCore.Identity;

namespace IP.DAL.EF;

public class DbInit
{
    private readonly AppDbContext _context;

    public DbInit(AppDbContext context)
    {
        _context = context;
    }

    public void Run()
    {
        Console.WriteLine("Initialising database...");
        _context.Database.EnsureDeleted();
        _context.Database.EnsureCreated();

        Seed(_context);
    }

    public static void Seed(AppDbContext appDbContext)
    {
        var platform1 = new Platform { Name = "Local Government Antwerpen", Projects = new List<Project>() };
        var platform2 = new Platform { Name = "Local Government Brussel", Projects = new List<Project>() };
        var platform3 = new Platform { Name = "Local Government Luik", Projects = new List<Project>() };

        appDbContext.Platforms.Add(platform1);
        appDbContext.Platforms.Add(platform2);
        appDbContext.Platforms.Add(platform3);

        var project1 = new Project
            { Title = "Verkiezingen", Description = "Vragen over de lokale verkiezingen", Flows = new List<Flow>() };
        var project2 = new Project
            { Title = "Sporten", Description = "Vragen over de sporten", Flows = new List<Flow>() };
        var project3 = new Project
            { Title = "Educatie", Description = "Vragen over de educatie", Flows = new List<Flow>() };

        appDbContext.Projects.Add(project1);
        appDbContext.Projects.Add(project2);
        appDbContext.Projects.Add(project3);

        platform1.Projects.Add(project1);

        platform2.Projects.Add(project2);
        platform2.Projects.Add(project3);

        var flow1 = new Flow
            { Name = "Flow 1", Type = "0", Description = "Flow Circ", Subthemes = new List<Subtheme>() };
        var flow2 = new Flow
            { Name = "Flow 2", Type = "1", Description = "Flow Lin", Subthemes = new List<Subtheme>() };
        var flow3 = new Flow
            { Name = "Flow 3", Type = "0", Description = "Flow Circ", Subthemes = new List<Subtheme>() };
        var flow4 = new Flow
            { Name = "Flow 4", Type = "1", Description = "Flow Lin", Subthemes = new List<Subtheme>() };

        appDbContext.Flows.Add(flow1);
        appDbContext.Flows.Add(flow2);
        appDbContext.Flows.Add(flow3);
        appDbContext.Flows.Add(flow4);

        project1.Flows.Add(flow1);
        project1.Flows.Add(flow2);

        project2.Flows.Add(flow3);
        project3.Flows.Add(flow4);

        var info1 = new InfoComponent()
        {
            Id = Guid.NewGuid(),
            Text =
                "In deze vragenlijst willen we graag jouw mening peilen over verschillende aspecten van het lokale gemeenschapsbeleid. Geen zorgen, het is geen saaie opsomming van feiten. We zijn gewoon benieuwd naar jouw gedachten en voorkeuren op een ontspannen manier.\n\nWe stellen vragen over onderwerpen zoals sport, vrije tijd, mobiliteit, huisvesting en nog veel meer. Ook komen er vragen over verkiezingen en hoe jij je betrokken voelt bij het beleid in jouw stad of gemeente.\n\n" +
                "Dus, laten we samen deze vragenlijst doorlopen en jouw stem laten horen over wat jij belangrijk vindt voor jouw buurt!"
        };

        var info2 = new InfoComponent()
        {
            Id = Guid.NewGuid(),
            Text = "Leg ons even uit waarom je (niet) wilt stemmen"
        };

        var info3 = new InfoComponent()
        {
            Id = Guid.NewGuid(),
            Text = "Hoe voel je bij de verkiezingen"
        };

        var info4 = new InfoComponent()
        {
            Id = Guid.NewGuid(),
            Text = "Nog iets"
        };

        var subtheme1 = new Subtheme
        {
            Name = "Kiesintenties", Description = "Subthema over de lokale verkiezingen",
            FlowItems = new List<FlowItem>(), Info = info1
        };
        var subtheme2 = new Subtheme
        {
            Name = "Redenen om (niet) te gaan stemmen", Description = "Subthema over de lokale verkiezingen",
            FlowItems = new List<FlowItem>(), Info = info2
        };
        var subtheme3 = new Subtheme
        {
            Name = "Gevoel van betrokkenheid bij lokaal beleid", Description = "Subthema over de lokale verkiezingen",
            FlowItems = new List<FlowItem>(), Info = info3
        };
        var subtheme4 = new Subtheme
        {
            Name = "Participatie aan de uitwerking van het lokaal beleid",
            Description = "Subthema over de lokale verkiezingen", FlowItems = new List<FlowItem>(),
            Info = info4
        };

        appDbContext.Subthemes.Add(subtheme1);
        appDbContext.Subthemes.Add(subtheme2);
        appDbContext.Subthemes.Add(subtheme3);
        appDbContext.Subthemes.Add(subtheme4);

        flow1.Subthemes.Add(subtheme1);
        flow2.Subthemes.Add(subtheme1);
        flow2.Subthemes.Add(subtheme2);

        flow3.Subthemes.Add(subtheme3);
        flow4.Subthemes.Add(subtheme4);


        var singleChoiceQuestion1 = new Question
        {
            Type = QuestionType.Single,
            Prompt =
                "Er moet meer geïnvesteerd worden in overdekte fietsstallingen aan de bushaltes in onze gemeente.\" Wat vind jij van dit voorstel?",
            Options = new List<Option>()
            {
                new Option() { Id = Guid.NewGuid(), Value = 1, Label = "Eens" },
                new Option() { Id = Guid.NewGuid(), Value = 0, Label = "Oneens" }
            }
        };
        var singleChoiceQuestion2 = new Question
        {
            Type = QuestionType.Single,
            Prompt = "Hoe sta jij tegenover deze stelling? “Mijn stad moet meer investeren in fietspaden”",
            Options = new List<Option>()
            {
                new Option() { Id = Guid.NewGuid(), Value = 1, Label = "Eens" },
                new Option() { Id = Guid.NewGuid(), Value = 0, Label = "Oneens" }
            }
        };
        var singleChoiceQuestion3 = new Question
        {
            Type = QuestionType.Single,
            Prompt =
                "Wat vind jij van het idee om alle leerlingen van de scholen in onze stad een gratis fiets aan te bieden?",
            Options = new List<Option>()
            {
                new Option() { Id = Guid.NewGuid(), Value = 1, Label = "Eens" },
                new Option() { Id = Guid.NewGuid(), Value = 0, Label = "Oneens" }
            }
        };
        
        //multiple choice questions  
        var multipleChoiceQuestion1 = new Question()
        {
            Id = Guid.NewGuid(),
            Type = QuestionType.Multiple,
            Prompt = "Wat zou jou helpen om een keuze te maken tussen de verschillende partijen?",
            Options = new List<Option>()
            {
                new Option
                {
                    Id = Guid.NewGuid(), Value = 0, Label = "Meer lessen op school rond de partijprogramma’s"
                },
                new Option
                {
                    Id = Guid.NewGuid(), Value = 1,
                    Label = "Activiteiten in mijn jeugdclub, sportclub… rond de verkiezingen"
                },
                new Option
                {
                    Id = Guid.NewGuid(), Value = 3,
                    Label = "Een bezoek van de partijen aan mijn school, jeugd/sportclub, …"
                },
                new Option
                {
                    Id = Guid.NewGuid(), Value = 4, Label = "Een gesprek met mijn ouders rond de gemeentepolitiek"
                },
                new Option
                {
                    Id = Guid.NewGuid(), Value = 5,
                    Label = "Een debat georganiseerd door een jeugdhuis met de verschillende partijen"
                }
            }
        };
        var multipleChoiceQuestion2 = new Question
        {
            Type = QuestionType.Multiple,
            Prompt = "Welke sportactiviteit(en) zou jij graag in je eigen stad of gemeente kunnen beoefenen?",
            Options = new List<Option>()
            {
                new Option { Id = Guid.NewGuid(), Value = 0, Label = "Tennis" },
                new Option { Id = Guid.NewGuid(), Value = 1, Label = "Hockey" },
                new Option { Id = Guid.NewGuid(), Value = 2, Label = "Padel" },
                new Option { Id = Guid.NewGuid(), Value = 3, Label = "Voetbal" },
                new Option { Id = Guid.NewGuid(), Value = 4, Label = "Fitness" }
            }
        };
        var multipleChoiceQuestion3 = new Question
        {
            Type = QuestionType.Multiple,
            Prompt =
                "Aan welke van deze activiteiten zou jij meedoen, om mee te wegen op het beleid van jouw stad of gemeente?",
            Options = new List<Option>()
            {
                new Option
                {
                    Id = Guid.NewGuid(), Value = 0,
                    Label = "Deelnemen aan gespreksavonden met schepenen en burgemeester"
                },
                new Option { Id = Guid.NewGuid(), Value = 1, Label = "Bijwonen van een gemeenteraad" },
                new Option
                {
                    Id = Guid.NewGuid(), Value = 2,
                    Label = "Deelnemen aan een survey uitgestuurd door de stad of gemeente"
                },
                new Option
                {
                    Id = Guid.NewGuid(), Value = 3,
                    Label = "Een overleg waarbij ik onderwerpen kan aandragen die voor jongeren belangrijk zijn"
                },
                new Option
                {
                    Id = Guid.NewGuid(), Value = 4,
                    Label = "Mee brainstormen over oplossingen voor problemen waar jongeren mee worstelen"
                }
            }
        };
        var multipleChoiceQuestion4 = new Question
        {
            Type = QuestionType.Multiple,
            Prompt = "Jij gaf aan dat je waarschijnlijk niet zal gaan stemmen. Om welke reden(en) zeg je dit?",
            Options = new List<Option>()
            {
                new Option { Id = Guid.NewGuid(), Value = 0, Label = "Ik heb geen interesse" },
                new Option { Id = Guid.NewGuid(), Value = 1, Label = "Ik heb geen tijd om te gaan stemmen" },
                new Option { Id = Guid.NewGuid(), Value = 2, Label = "Ik kan niet naar het stemkantoor gaan" },
                new Option
                {
                    Id = Guid.NewGuid(), Value = 3, Label = "Ik denk niet dat mijn stem een verschil zal uitmaken"
                },
                new Option { Id = Guid.NewGuid(), Value = 4, Label = "Ik heb geen idee voor wie ik zou moeten stemmen" }
            }
        };
        var multipleChoiceQuestion5 = new Question
        {
            Type = QuestionType.Multiple, Prompt = "Wat zou jou (meer) zin geven om te gaan stemmen?",
            Options = new List<Option>()
            {
                new Option { Id = Guid.NewGuid(), Value = 0, Label = "Kunnen gaan stemmen op een toffere locatie" },
                new Option { Id = Guid.NewGuid(), Value = 1, Label = "Online, van thuis uit kunnen stemmen" },
                new Option
                {
                    Id = Guid.NewGuid(), Value = 2, Label = "Betere inhoudelijke voorstellen van de politieke partijen"
                },
                new Option
                {
                    Id = Guid.NewGuid(), Value = 3, Label = "Meer aandacht voor jeugd in de programma’s van de partijen"
                },
                new Option { Id = Guid.NewGuid(), Value = 4, Label = "Beter weten of mijn stem echt impact heeft" }
            }
        };

        var openQuestion1 = new Question
        {
            Type = QuestionType.Open,
            Prompt = "Je bent schepen van onderwijs voor een dag: waar zet je dan vooral op in?",
            Options = new List<Option>()
        };
        var openQuestion2 = new Question
        {
            Type = QuestionType.Open,
            Prompt =
                "Als je één ding mag wensen voor het nieuwe stadspark, wat zou jouw droomstadspark dan zeker bevatten?",
            Options = new List<Option>()
        };

        var rangeQuestion1 = new Question
        {
            Type = QuestionType.Range,
            Prompt = "Ben jij van plan om te gaan stemmen bij de aankomende lokale verkiezingen?",
            Options = new List<Option>()
            {
                new Option { Id = Guid.NewGuid(), Value = 0, Label = "Zeker niet" },
                new Option { Id = Guid.NewGuid(), Value = 1, Label = "Eerder niet" },
                new Option { Id = Guid.NewGuid(), Value = 2, Label = "Ik weet het nog niet" },
                new Option { Id = Guid.NewGuid(), Value = 3, Label = "Eerder wel" },
                new Option { Id = Guid.NewGuid(), Value = 4, Label = "Zeker wel" }
            }
        };
        var rangeQuestion2 = new Question
        {
            Type = QuestionType.Range,
            Prompt = "In hoeverre ben jij tevreden met het vrijetijdsaanbod in jouw stad of gemeente?",
            Options = new List<Option>()
            {
                new Option { Id = Guid.NewGuid(), Value = 0, Label = "Zeer ontevreden" },
                new Option { Id = Guid.NewGuid(), Value = 1, Label = "Ontevreden" },
                new Option { Id = Guid.NewGuid(), Value = 2, Label = "Niet tevreden en niet ontevreden" },
                new Option { Id = Guid.NewGuid(), Value = 3, Label = "Tevreden" },
                new Option { Id = Guid.NewGuid(), Value = 4, Label = "Zeer tevreden" }
            }
        };
        var rangeQuestion3 = new Question
        {
            Type = QuestionType.Range,
            Prompt =
                "In welke mate ben jij het eens met de volgende stelling: “Mijn stad of gemeente doet voldoende om betaalbare huisvesting mogelijk te maken voor iedereen.”",
            Options = new List<Option>()
            {
                new Option { Id = Guid.NewGuid(), Value = 0, Label = "Helemaal oneens" },
                new Option { Id = Guid.NewGuid(), Value = 1, Label = "Oneens" },
                new Option { Id = Guid.NewGuid(), Value = 2, Label = "Noch eens, noch oneens" },
                new Option { Id = Guid.NewGuid(), Value = 3, Label = "Eens" },
                new Option { Id = Guid.NewGuid(), Value = 4, Label = "Helemaal eens" }
            }
        };


        appDbContext.Questions.Add(singleChoiceQuestion1);
        appDbContext.Questions.Add(singleChoiceQuestion2);
        appDbContext.Questions.Add(singleChoiceQuestion3);
        appDbContext.Questions.Add(multipleChoiceQuestion1);
        appDbContext.Questions.Add(multipleChoiceQuestion2);
        appDbContext.Questions.Add(multipleChoiceQuestion3);
        appDbContext.Questions.Add(multipleChoiceQuestion4);
        appDbContext.Questions.Add(multipleChoiceQuestion5);
        appDbContext.Questions.Add(openQuestion1);
        appDbContext.Questions.Add(openQuestion2);
        appDbContext.Questions.Add(rangeQuestion1);
        appDbContext.Questions.Add(rangeQuestion2);
        appDbContext.Questions.Add(rangeQuestion3);

        subtheme1.FlowItems.Add(singleChoiceQuestion1);
        subtheme1.FlowItems.Add(singleChoiceQuestion2);
        subtheme1.FlowItems.Add(singleChoiceQuestion3);
        subtheme1.FlowItems.Add(multipleChoiceQuestion1);
        subtheme1.FlowItems.Add(multipleChoiceQuestion2);
        subtheme1.FlowItems.Add(multipleChoiceQuestion3);
        subtheme1.FlowItems.Add(multipleChoiceQuestion4);
        subtheme1.FlowItems.Add(multipleChoiceQuestion5);
        subtheme1.FlowItems.Add(openQuestion1);
        subtheme1.FlowItems.Add(openQuestion2);
        subtheme1.FlowItems.Add(rangeQuestion1);
        subtheme1.FlowItems.Add(rangeQuestion2);
        subtheme1.FlowItems.Add(rangeQuestion3);

        subtheme2.FlowItems.Add(singleChoiceQuestion1);
        subtheme2.FlowItems.Add(multipleChoiceQuestion1);
        subtheme2.FlowItems.Add(openQuestion1);
        subtheme2.FlowItems.Add(rangeQuestion1);

        subtheme3.FlowItems.Add(singleChoiceQuestion2);
        subtheme3.FlowItems.Add(multipleChoiceQuestion2);
        subtheme3.FlowItems.Add(openQuestion2);
        subtheme3.FlowItems.Add(rangeQuestion2);
        subtheme3.FlowItems.Add(multipleChoiceQuestion4);

        subtheme4.FlowItems.Add(singleChoiceQuestion3);
        subtheme4.FlowItems.Add(multipleChoiceQuestion3);
        subtheme4.FlowItems.Add(rangeQuestion3);


        var idea1 = new Idea()
        {
            Id = Guid.NewGuid(),
            Title = "Jongerenkunstpodium",
            Text =
                "Organiseer regelmatig open mic-avonden en kunsttentoonstellingen voor jongeren om hun creativiteit te laten zien."
        };

        var idea2 = new Idea()
        {
            Id = Guid.NewGuid(),
            Title = "Jongerenpark",
            Text =
                "Creëer een speciale groene ruimte waar jongeren kunnen sporten, ontspannen en sociale contacten kunnen leggen."
        };

        var idea3 = new Idea()
        {
            Id = Guid.NewGuid(),
            Title = "Jongeren-ondernemerschapsprogramma",
            Text =
                "Start een programma om jongeren te helpen bij het ontwikkelen van ondernemersvaardigheden en het opzetten van hun eigen bedrijven.\n\n\n\n\n\n\n"
        };
        
        appDbContext.Ideas.Add(idea1);
        appDbContext.Ideas.Add(idea2);
        appDbContext.Ideas.Add(idea3);

        appDbContext.SaveChanges();
    }
}