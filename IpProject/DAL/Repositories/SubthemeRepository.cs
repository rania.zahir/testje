using IP.BL.Domain.Flows.FlowComponents;
using IP.BL.Domain.Flows.FlowComponents.Questions;
using IP.DAL.API;
using IP.DAL.EF;
using Microsoft.EntityFrameworkCore;

namespace IP.DAL.Repositories;

public class SubthemeRepository : ISubthemeRepository
{
    private readonly AppDbContext _context;

    public SubthemeRepository(AppDbContext context)
    {
        _context = context;
    }

    public Subtheme ReadSubtheme(Guid subthemeId)
    {
        return _context.Subthemes
            .Include(subtheme => subtheme.Info)
            .FirstOrDefault(subtheme => subtheme.Id == subthemeId);
    }


    public FlowItem ReadNextFlowItem(Guid flowId)
    {
        throw new NotImplementedException();
    }

    public IEnumerable<Question> ReadQuestionsWithOptions(Guid subthemeId, string mode)
    {
        var subtheme = _context.Subthemes
            .Include(st => st.FlowItems)
            .ThenInclude(item => (item as Question).Options)
            .FirstOrDefault(st => st.Id == subthemeId);

        if (subtheme == null)
        {
            return null;
        }

        if (mode == "phygital")
        {
            return subtheme.FlowItems.OfType<Question>()
                .Where(question => question.Type != QuestionType.Open)
                .ToList();
        }
        else
        {
            var list = subtheme.FlowItems.OfType<Question>().ToList();
            return list;
        }
    }

    public void CreateSubtheme(Subtheme subtheme)
    {
        _context.Subthemes.Add(subtheme);
        _context.SaveChanges();
    }

    public IEnumerable<Subtheme> ReadAllSubthemes()
    {
        return _context.Subthemes
            .Include(subtheme => subtheme.Info)
            .ToList();
    }

    public void ConnectSubthemeToFlow(Guid flowId, Guid subthemeId)
    {
        var flow = _context.Flows.Find(flowId);
        var subtheme = _context.Subthemes.Find(subthemeId);

        if (flow != null && subtheme != null)
        {
            flow.Subthemes.Add(subtheme);
            _context.SaveChanges();
        }
    }
}