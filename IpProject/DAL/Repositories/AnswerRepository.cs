﻿using IP.BL.Domain.Flows.FlowComponents.Questions;
using IP.DAL.API;
using IP.DAL.EF;

namespace IP.DAL.Repositories;

public class AnswerRepository : IAnswerRepository
{
    private readonly AppDbContext _context;

    public AnswerRepository(AppDbContext appDbContext)
    {
        _context = appDbContext;
    }

    public void CreateAnswer(Answer answer)
    {
        _context.Answers.Add(answer);
    }
}