using IP.BL.Domain.Flows.FlowComponents.Questions;
using IP.DAL.API;
using IP.DAL.EF;
using Microsoft.EntityFrameworkCore;

namespace IP.DAL.Repositories;

public class QuestionRepository : IQuestionRepository
{
    private readonly AppDbContext _context;

    public QuestionRepository(AppDbContext context)
    {
        _context = context;
    }
    
    public Question ReadQuestion(Guid questionId)
    {
        return _context.FlowItems.OfType<Question>().Single(q => q.Id == questionId);
    }

    public Question ReadQuestionWithOptions(Guid questionId)
    {
        return _context.FlowItems
            .OfType<Question>()
            .Include(q => q.Options)
            .Single(q => q.Id == questionId);
    }

    public void UpdateOptions(ICollection<Option> answerOptions)
    {
        {
            foreach (var option in answerOptions)
            {
                var existingOption = _context.Options.FirstOrDefault(o => o.Id == option.Id);

                if (existingOption != null)
                {
                    existingOption = option;
                    _context.Update(existingOption);
                }
            }
            
            _context.SaveChanges();
        }
    }
}