﻿using IP.BL.Domain;
using IP.BL.Domain.Flows;
using IP.BL.Domain.Flows.FlowComponents;
using IP.BL.Domain.Flows.FlowComponents.Questions;
using IP.BL.Domain.User;
using IP.DAL.API;
using IP.DAL.EF;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace IP.DAL.Repositories;

public class FlowRepository : IFlowRepository
{
    private readonly AppDbContext _context;

    public FlowRepository(AppDbContext context)
    {
        _context = context;
    }

    public Flow ReadFlow(Guid flowId)
    {
        return _context.Flows.Find(flowId);
    }

    // public IEnumerable<Question> ReadQuestionsWithOptions(Guid flowId)
    // {
    //     ICollection<Question> questions = new List<Question>();
    //     Flow flow = ReadFlow(flowId);
    //
    //     if (flow != null)
    //     {
    //         foreach (var item in _context.FlowItems)
    //         {
    //             if (item is Question question)
    //             {
    //                 question.Options = _context.Options.Where(opt => opt.Id == question.Id).ToList();
    //                 questions.Add(question);
    //             }
    //         } 
    //     }
    //     else
    //     {
    //         return null;
    //     }
    //
    //     return questions;
    // }

    public IEnumerable<Question> ReadQuestionsWithOptions(Guid flowId)
    {
        var flow = ReadFlow(flowId);

        if (flow == null) return null;
        var questions = _context.FlowItems
            .OfType<Question>()
            .Include(q => q.Options)
            .ToList();

        return questions;
    }

    public Question ReadQuestion(Guid questionId)
    {
        return _context.FlowItems.OfType<Question>().Single(q => q.Id == questionId);
    }

    public Question ReadQuestionWithOptions(Guid questionId)
    {
        return _context.FlowItems
            .OfType<Question>()
            .Include(q => q.Options)
            .Single(q => q.Id == questionId);
    }

    public void CreateFlow(Flow flow)
    {
        _context.Flows.Add(flow);
        _context.SaveChanges();
    }

    public Flow ReadFlowWithSubthemes(Guid flowId)
    {
        return _context.Flows
            .Include(flow => flow.Subthemes)
            .Single(flow => flow.Id == flowId);
    }

    public void CreateQuestion(Question question)
    {
        _context.Questions.Add(question);

        if (question.Options != null)
        {
            foreach (var option in question.Options)
            {
                _context.Options.Add(option);
            }
        }

        _context.SaveChanges();
    }

    public IEnumerable<Question> ReadAllQuestions()
    {
        return _context.Questions;
    }

    public IEnumerable<Question> ReadAllQuestionsWithOptions()
    {
        return _context.Questions.Include(q => q.Options);
    }

    public void DeleteQuestion(Question question)
    {
        _context.Questions.Remove(question);
        _context.SaveChanges();
    }

    public Question ReadQuestionById(Guid id)
    {
        return _context.Questions.Find(id);
    }

    public IEnumerable<Project> ReadAllProjectsWithPlatform()
    {
        return _context.Projects.ToList();
    }

    public void CreateProject(Project project)
    {
        _context.Projects.Add(project);
        _context.SaveChanges();
    }

    public IEnumerable<Platform> ReadAllPlatforms()
    {
        return _context.Platforms.ToList();
    }

    public IEnumerable<Flow> ReadAllFlows()
    {
        return _context.Flows.ToList();
    }

    public IEnumerable<Subtheme> ReadAllSubthemes()
    {
        return _context.Subthemes
            .Include(subtheme => subtheme.Info)
            .ToList();
    }

    public void CreatePlatform(Platform platform)
    {
        _context.Platforms.Add(platform);
        _context.SaveChanges();
    }

    public IEnumerable<Subtheme> ReadSubthemes(Guid flowId)
    {
        return _context.Subthemes.Where(subtheme => subtheme.FlowId == flowId).ToList();
    }

    /*public Flow ReadFlowWithSubthemes(Guid flowId)
    {
        return _context.Flows.Include(flow => flow.Subthemes).Single(flow => flow.Id == flowId);
    }*/
}