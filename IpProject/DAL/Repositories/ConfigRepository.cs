﻿namespace IP.DAL.Repositories;

public static class ConfigRepository
{
    private const string ConfigFileName = "configmodes.txt";

    public static string LoadMode()
    {
        return File.Exists(ConfigFileName) ? File.ReadAllText(ConfigFileName) : "kiosk";
    }

    public static void SaveMode(string mode)
    {
        File.WriteAllText(ConfigFileName, mode);
    }
}