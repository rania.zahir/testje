﻿using IP.BL.Domain.Flows;
using IP.BL.Domain.Ideas;
using IP.DAL.API;
using IP.DAL.EF;

namespace IP.DAL.Repositories;

public class IdeasRepository: IIdeasRepository
{
    private readonly AppDbContext _context;

    public IdeasRepository(AppDbContext context)
    {
        _context = context;
    }
    
    public Idea ReadIdea(Guid ideaId)
    {
        return _context.Ideas.Find(ideaId);
    }
    
    public IEnumerable<Idea> ReadIdeas()
    {
        return _context.Ideas;
    }
}