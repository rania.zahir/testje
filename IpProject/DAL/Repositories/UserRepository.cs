﻿using IP.BL.Domain.User;
using IP.DAL.API;
using IP.DAL.EF;

namespace IP.DAL.Repositories;

public class UserRepository: IUserRepository
{
    private readonly AppDbContext _context;
    
    public UserRepository(AppDbContext context)
    {
        _context = context;
    }

    public void Create(User user)
    {
        _context.Users.Add(user);
        _context.SaveChanges();
    }
    public Platform ReadPlatform(Guid platformId)
    {
        return _context.Platforms.Find(platformId);
    }
}