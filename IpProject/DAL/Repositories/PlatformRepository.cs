using IP.BL.Domain.User;
using IP.DAL.API;
using IP.DAL.EF;
using Microsoft.EntityFrameworkCore;

namespace IP.DAL.Repositories;

public class PlatformRepository : IPlatformRepository
{
    private readonly AppDbContext _context;    
    public PlatformRepository(AppDbContext context)
    {
        _context = context;
    }

    public async Task<IEnumerable<Platform>> GetPlatformsAsync()
    {
        return await _context.Platforms.ToListAsync();
    }

    public async Task<Platform?> GetPlatformByIdAsync(int id)
    {
        return await _context.Platforms.FindAsync(id);
    }

    public async Task AddPlatformAsync(Platform platform)
    {
        await _context.Platforms.AddAsync(platform);
        await _context.SaveChangesAsync();
    }

    public async Task UpdatePlatformAsync(Platform platform)
    {
        _context.Platforms.Update(platform);
        await _context.SaveChangesAsync();
    }

    public async Task DeletePlatformAsync(Platform platform)
    {
        _context.Platforms.Remove(platform);
        await _context.SaveChangesAsync();
    }
}