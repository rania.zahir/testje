﻿using IP.BL.Domain.Flows.FlowComponents.Questions;

namespace IP.DAL.API;

public interface IAnswerRepository
{
    void CreateAnswer(Answer answer);
}