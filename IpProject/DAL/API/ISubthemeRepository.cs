using IP.BL.Domain.Flows.FlowComponents;
using IP.BL.Domain.Flows.FlowComponents.Questions;

namespace IP.DAL.API;

public interface ISubthemeRepository
{
    Subtheme ReadSubtheme(Guid subthemeId);
    FlowItem ReadNextFlowItem(Guid flowId);
    //IEnumerable<Question> ReadQuestionsWithOptions(Guid flowId);
    IEnumerable<Question> ReadQuestionsWithOptions(Guid subthemeId,string mode);
    void CreateSubtheme(Subtheme subtheme);
    IEnumerable<Subtheme> ReadAllSubthemes();
    void ConnectSubthemeToFlow(Guid flowId, Guid subthemeId);
}