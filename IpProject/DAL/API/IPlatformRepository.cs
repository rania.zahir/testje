using IP.BL.Domain.User;

namespace IP.DAL.API;

public interface IPlatformRepository
{
    Task<IEnumerable<Platform>> GetPlatformsAsync();
    Task<Platform?> GetPlatformByIdAsync(int id);
    Task AddPlatformAsync(Platform platform);
    Task UpdatePlatformAsync(Platform platform);
    Task DeletePlatformAsync(Platform platform);
}