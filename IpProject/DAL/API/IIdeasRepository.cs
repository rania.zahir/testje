﻿using IP.BL.Domain.Ideas;

namespace IP.DAL.API;

public interface IIdeasRepository
{
    Idea ReadIdea(Guid ideaId);
    IEnumerable<Idea> ReadIdeas();
}