﻿using IP.BL.Domain;
using IP.BL.Domain.Flows;
using IP.BL.Domain.Flows.FlowComponents;
using IP.BL.Domain.Flows.FlowComponents.Questions;
using IP.BL.Domain.User;
using Microsoft.AspNetCore.Identity;

namespace IP.DAL.API;

public interface IFlowRepository
{
    Flow ReadFlow(Guid flowId);
    void CreateFlow(Flow flow);
    Flow ReadFlowWithSubthemes(Guid flowId);
    void CreateQuestion(Question question);
    IEnumerable<Question> ReadAllQuestions();
    IEnumerable<Question> ReadAllQuestionsWithOptions();
    void DeleteQuestion(Question question);
    Question ReadQuestionById(Guid id);
    IEnumerable<Project> ReadAllProjectsWithPlatform();
    void CreateProject(Project project);
    IEnumerable<Platform> ReadAllPlatforms();
    IEnumerable<Flow> ReadAllFlows();
    
    IEnumerable<Subtheme> ReadAllSubthemes();
    void CreatePlatform(Platform platform);
    IEnumerable<Subtheme> ReadSubthemes(Guid flowId);
}