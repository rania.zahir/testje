using IP.BL.Domain.Flows.FlowComponents.Questions;

namespace IP.DAL.API;

public interface IQuestionRepository
{
    Question ReadQuestion(Guid questionId);
    Question ReadQuestionWithOptions(Guid questionId);
    void UpdateOptions(ICollection<Option> answerOptions);
}