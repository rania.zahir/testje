﻿using IP.BL.Domain.User;

namespace IP.DAL.API;

public interface IUserRepository
{
    void Create(User user);
    Platform ReadPlatform(Guid platformId);
}