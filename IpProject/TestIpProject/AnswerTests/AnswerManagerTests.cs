﻿using IP.BL.API;
using IP.BL.Domain.Flows.FlowComponents.Questions;
using IP.BL.Managers;
using IP.DAL.API;
using Moq;

namespace TestProject1.AnswerTests;

[TestClass]
public class AnwserManagerTests
{
    private Mock<IAnswerRepository> _mockAnswerRepository;
    private IAnswerManager _answerManager;

    [TestInitialize]
    public void Setup()
    {
        // Initialize mock IAnswerRepository
        _mockAnswerRepository = new Mock<IAnswerRepository>();

        // Initialize AnswerManager with mock IAnswerRepository
        // _answerManager = new AnswerManager(_mockAnswerRepository.Object);
    }

    [TestMethod]
    public void AddAnswer_CallsCreateAnswer_WithCorrectAnswer()
    {
        // Arrange
        var answer = new Answer
        {
            Id = Guid.NewGuid(),
            // Set other properties as needed
        };

        // Act
        _answerManager.AddAnswer(answer);

        // Assert
        _mockAnswerRepository.Verify(m => m.CreateAnswer(answer), Times.Once);
    }

    // Add more test methods as needed...
}