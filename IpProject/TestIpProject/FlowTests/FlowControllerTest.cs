﻿// using IP.BL.API;
// using IP.BL.Domain.Flows.FlowComponents;
// using IP.BL.Domain.Flows.FlowComponents.Questions;
// using IP.BL.Managers;
// using IpProject.UI.MVC.Controllers;
// using IpProject.UI.MVC.Controllers.api;
// using IpProject.UI.MVC.Models.Dtos;
// using Microsoft.AspNetCore.Mvc;
// using Moq;
//
// namespace TestProject1.FlowTests;
//
// [TestClass]
// public class FlowControllerTests
// {
//     private Mock<IFlowManager> _mockFlowManager;
//     private Mock<IAnswerManager> _mockAnswerManager;
//     private FlowsController _flowsController;
//
//     [TestInitialize]
//     public void Setup()
//     {
//         _mockFlowManager = new Mock<IFlowManager>();
//         _mockAnswerManager = new Mock<IAnswerManager>();
//         _flowsController = new FlowsController(_mockFlowManager.Object, _mockAnswerManager.Object,null);
//     }
//     
//
//     [TestMethod]
//     public void SubmitAnswer_ReturnsOk_WhenModelStateIsValid()
//     {
//         var answer = new Answer();
//         _flowsController.ModelState.Clear(); 
//
//         var result = _flowsController.SubmitAnswer(new AnswerDto());
//
//         Assert.IsInstanceOfType(result, typeof(OkResult));
//         _mockAnswerManager.Verify(m => m.AddAnswer(answer), Times.Once);
//     }
//
//     [TestMethod]
//     public void SubmitAnswer_ReturnsBadRequest_WhenModelStateIsInvalid()
//     {
//         _flowsController.ModelState.AddModelError("Name", "Error message");
//
//         // var result = _flowsController.SubmitAnswer(new AnswerDto());
//
//         // Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
//     }
// }